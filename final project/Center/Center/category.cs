﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Center
{
    public partial class categories : Form
    {
        BindingManagerBase bm;
      

        public categories()
        {
            InitializeComponent();
        }

        private void categories_Load(object sender, EventArgs e)
        {
            using (var _db = new Datacontext())
            {
                var data = _db._Categories
                    .Select(x => new { x.ID, x.Type });
                categgrid.DataSource = data.ToList();
                textdes.DataBindings.Add("Text", categgrid.DataSource, "Type");
                bm = this.BindingContext[categgrid.DataSource];
                categPoss.Text = (bm.Position + 1) + "  /  " + bm.Count;


            }
            
        }

        private void categPoss_Click(object sender, EventArgs e)
        {

        }

        private void first_Click(object sender, EventArgs e)
        {
            bm.Position = 0;
            categPoss.Text = (bm.Position + 1) + "  /  " + bm.Count;
        }

        private void last_Click(object sender, EventArgs e)
        {
            bm.Position = bm.Count;
            categPoss.Text = (bm.Position + 1) + "  /  " + bm.Count;

        }

        private void prevous_Click(object sender, EventArgs e)
        {
            bm.Position -= 1;
            categPoss.Text = (bm.Position + 1) + "  /  " + bm.Count;
        }

        private void next_Click(object sender, EventArgs e)
        {
            bm.Position += 1;
            categPoss.Text = (bm.Position + 1) + "  /  " + bm.Count;
        }

        private void categnew_Click(object sender, EventArgs e)
        {


            textdes.Text = " ";
                categnew.Enabled = false;
                categnew.Enabled = true;
                textdes.Focus();
            
               
        }

        private void categadd_Click(object sender, EventArgs e)
        {
            bm.EndCurrentEdit();
            using (var _db = new Datacontext())
            {
                Random prid = new Random();
                int prid1 = prid.Next(1000, 9999);
                Categories ex = _db._Categories.SingleOrDefault(pr => pr.ID == prid1);
                while (ex != null)
                {
                    prid1 = prid.Next(1000, 9999);
                    ex = _db._Categories.SingleOrDefault(pr => pr.ID == prid1);
                }
                Categories c1 = new Categories
                {
                    ID = prid1,
                    Type = textdes.Text

                };
                _db._Categories.Add(c1);
                _db.SaveChanges();
                categgrid.DataSource = _db._Categories.ToList();
                bm = this.BindingContext[categgrid.DataSource];
                categPoss.Text = (bm.Position + 1) + "  /  " + bm.Count;


            }




        }

        private void deletecateg_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("are you sure the you want to delete this category", "delete category", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
            {
                using (var _db = new Datacontext())
                {
                    int rowindex = categgrid.CurrentCell.RowIndex;
                    int columnindex = categgrid.CurrentCell.ColumnIndex;
                    int id = Convert.ToInt32(categgrid.Rows[rowindex].Cells[0].Value.ToString());
                    foreach (Categories ca in _db._Categories)
                    {
                        if (ca.ID == id)
                        {
                            _db._Categories.Remove(ca);
                            break;
                        }
                    }
                    bm.EndCurrentEdit();
                    // Save Changes
                    MessageBox.Show("category has deleted", "delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _db.SaveChanges();
                    categgrid.DataSource = _db._Categories.ToList();
                    bm = this.BindingContext[categgrid.DataSource];
                    categPoss.Text = (bm.Position + 1) + "  /  " + bm.Count;
                }
               

            }

        }

        private void button8_Click(object sender, EventArgs e)
        {
            bm.EndCurrentEdit();
            using (var _db = new Datacontext())
            {
                int rowindex = categgrid.CurrentCell.RowIndex;
                int columnindex = categgrid.CurrentCell.ColumnIndex;
                int ID = Convert.ToInt32(categgrid.Rows[rowindex].Cells[0].Value);
                var ca = _db._Categories.SingleOrDefault(p => p.ID == ID);
                ca.Type = textdes.Text;
                _db.SaveChanges();
                MessageBox.Show("Updated!");
                categgrid.DataSource = _db._Categories.ToList();
                bm = this.BindingContext[categgrid.DataSource];
                categPoss.Text = (bm.Position + 1) + "  /  " + bm.Count;
            }

            }

        private void categgrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowindex = categgrid.CurrentCell.RowIndex;
            int columnindex = categgrid.CurrentCell.ColumnIndex;
            textdes.Text = categgrid.Rows[rowindex].Cells[1].Value.ToString();
            bm = this.BindingContext[categgrid.DataSource];
            categPoss.Text = (bm.Position + 1) + "  /  " + bm.Count;
        }
    }
}
    
