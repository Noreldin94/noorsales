﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Center
{
    public partial class All_Products : Form
    {
        public All_Products()
        {
            InitializeComponent();
        }

        private void All_Products_Load(object sender, EventArgs e)
        {
            using (var _db = new Datacontext())
            {
                
               
                
                var data = _db._product
                    .Select(x => new {x.Id,x.Name,x.Price,x.Quntity,x.Type });
               allproductbill.DataSource = data.ToList();
            }
        }

        private void allproductbillgrid_DoubleClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void allproductbill_DoubleClick(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
