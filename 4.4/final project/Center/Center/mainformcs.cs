﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Center
{
    public partial class mainformcs : Form
    {
        public string employeename;
        public string employeerole;
        public mainformcs(string username,string role)
        {
            employeename = username;
            employeerole = role;
            InitializeComponent();
        }

        private void newem_Click(object sender, EventArgs e)
        {
            register re = new register();
            re.Show();
            this.Hide();
        }

        private void mainformcs_Load(object sender, EventArgs e)
        {
            if(employeerole == "Admin")
            {
                newem.Visible = true;
            }
            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Products product = new Products();
            product.ShowDialog();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            categories frm = new categories();
            frm.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            customer frm = new customer();
            frm.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Order ord = new Order(employeename);
            ord.ShowDialog();
            

        }
    }
}
