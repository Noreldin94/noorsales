﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary1;
using System.IO;

namespace Center
{
    public partial class register : Form
    {
        OpenFileDialog opf;
        public register()
        {
            InitializeComponent();
        }

        private void Addnem_Click(object sender, EventArgs e)
        {


            using (var _db = new Datacontext())
            {

                Random emid = new Random();
                int emid1 = emid.Next(1000, 9999);
                Employee ex = _db._Employee.SingleOrDefault(p => p.ID == emid1);                      
                while (ex != null)
                {
                    emid1 = emid.Next(1000, 9999);
                    ex = _db._Employee.SingleOrDefault(p => p.ID == emid1);
                }
                /*
                string imagename = "";
                if (!string.IsNullOrEmpty(opf.FileName))
                {
                    imagename = opf.FileName;
              
                    File.Copy(imagename, @"Images\" + emid1 + ".jpg");
                }
                */
                Employee e1 = new Employee
                {
                    ID = emid1,
                    name = nameem.Text,
                    email = email.Text,
                    Passwor = password.Text,
                    Role = role.SelectedItem.ToString(),
                    IDentitynu = idnum.Text,
                    //Imagepath = imagename,
                    Gender = emgender.SelectedItem.ToString(),
                    Mobile = empmobile.Text           
                   
                };
                _db._Employee.Add(e1);
                _db.SaveChanges();
                empgrid.DataSource = _db._Employee.ToList();
                //MessageBox.Show("Done");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            opf = new OpenFileDialog();
            opf.Filter = "image files|*.jpg;*.png;*.gif;*.icon";
            DialogResult dr = opf.ShowDialog();
            if (dr == DialogResult.Cancel)
                return;
           // pictureBox1.Image = Image.FromFile(opf.FileName);
            //pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
           // pictureBox1.BorderStyle = BorderStyle.Fixed3D;
           

        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (var _db = new Datacontext())
            {
                var em = _db._Employee.ToList();
                empgrid.DataSource = em;
            }
            
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (var _db = new Datacontext())
            {
                int rowindex = empgrid.CurrentCell.RowIndex;
                int columnindex = empgrid.CurrentCell.ColumnIndex;
                int id = Convert.ToInt32(empgrid.Rows[rowindex].Cells[0].Value.ToString());
                foreach (Employee emp in _db._Employee)
                {
                    if (emp.ID == id)
                    {
                        _db._Employee.Remove(emp);
                        break;
                    }
                }
                // Save Changes
                _db.SaveChanges();
                button2.PerformClick();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(idnum.Text))
            {

                using (var _db = new Datacontext())
                {

                    int idem = Convert.ToInt32(idnum.Text);
                    var emp = _db._Employee.SingleOrDefault(p => p.IDentitynu == idnum.Text);
                    emp.name = nameem.Text;
                    emp.email = email.Text;
                    emp.Passwor = password.Text;
                    emp.Role = role.SelectedItem.ToString();
                    emp.Gender = emgender.SelectedItem.ToString();
                    emp.IDentitynu = idnum.Text;
                    //emp.Imagepath = opf.FileName;
                    emp.Mobile = empmobile.Text;
                    _db.SaveChanges();
                    MessageBox.Show("Updated!");
                    button2.PerformClick();
                }
            }
            else
                MessageBox.Show("Select an Employee!");
                

        }
 
        private void clearText()
        {
            password.Clear();
            idnum.Clear();
            nameem.Clear();
            email.Clear();
            role.SelectedIndex = 0;
            emgender.SelectedIndex = 0;
            empmobile.Clear();
        }
        
        private void empgrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowindex = empgrid.CurrentCell.RowIndex;
            int columnindex = empgrid.CurrentCell.ColumnIndex;
            nameem.Text = empgrid.Rows[rowindex].Cells[1].Value.ToString();
            email.Text = empgrid.Rows[rowindex].Cells[2].Value.ToString();
            empmobile.Text = empgrid.Rows[rowindex].Cells[3].Value.ToString();
            idnum.Text = empgrid.Rows[rowindex].Cells[4].Value.ToString();
            String roles = empgrid.Rows[rowindex].Cells[5].Value.ToString();
            if (roles == "Admin")
                role.SelectedIndex = 0;
            else
                role.SelectedIndex = 1;
            password.Text = empgrid.Rows[rowindex].Cells[6].Value.ToString();
            String gender = empgrid.Rows[rowindex].Cells[7].Value.ToString();
            if (gender == "Male")
                emgender.SelectedIndex = 0;
            else
                emgender.SelectedIndex = 1;

        }

        private void button5_Click(object sender, EventArgs e)
        {
            
            using (var _db = new Datacontext())
            {
              
                    if (nameem.Text != "")
                    {
                    var search = from mn in _db._Employee
                                 where mn.name.Contains(nameem.Text) || mn.email.Contains(nameem.Text) || mn.Mobile.Contains(nameem.Text)
                                 select mn;

                        empgrid.DataSource = search.ToList();
                    }
              
            }
                       
        }
    }
}
