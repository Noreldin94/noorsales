﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Center
{
    public partial class customer : Form
    {
        public customer()
        {
            InitializeComponent();
        }
        BindingManagerBase bm;
        private void customerimg_Click(object sender, EventArgs e)
        {
            OpenFileDialog opf = new OpenFileDialog();
            opf.Filter = "image files|*.jpg;*.png;*.gif;*.icon";
            DialogResult dr = opf.ShowDialog();


            if (dr == DialogResult.Cancel)
                return;
            customerimg.Image = Image.FromFile(opf.FileName);
            customerimg.SizeMode = PictureBoxSizeMode.StretchImage;
            customerimg.BorderStyle = BorderStyle.Fixed3D;
        }
        public void gridview()
        {
            using (var _db = new Datacontext())
            {
                var data = _db._Customers
                    .Select(x => new { x.ID, x.firstname, x.lastname, x.mobile, x.email });
                custgrid.DataSource = data.ToList();
            }
        }

        private void addcustomer_Click(object sender, EventArgs e)
        {
            if (custgrid.DataSource != null)
            {
                bm.EndCurrentEdit();
            }
                using (var _db = new Datacontext())
                {
                    if (customerimg.Image != null)
                    {
                        MemoryStream m = new MemoryStream();
                        customerimg.Image.Save(m, customerimg.Image.RawFormat);
                        byte[] byteimg = m.ToArray();
                        Random cusid = new Random();
                        int custid = cusid.Next(1000, 9999);
                        Customers ex = _db._Customers.SingleOrDefault(cus => cus.ID == custid);
                        while (ex != null)
                        {
                            custid = cusid.Next(1000, 9999);
                            ex = _db._Customers.SingleOrDefault(cus => cus.ID == custid);
                        }

                        Customers c1 = new Customers
                        {
                            ID = custid,
                            firstname = Custfirstname.Text,
                            lastname = custlastname.Text,
                            mobile = custmobile.Text,
                            email = custemail.Text,
                            image = byteimg,

                        };
                        _db._Customers.Add(c1);
                        _db.SaveChanges();
                        gridview();
                        bm = this.BindingContext[custgrid.DataSource];
                        custpossition.Text = (bm.Position + 1) + "  /  " + bm.Count;
                        addcustomer.Enabled = false;
                        newcustomer.Enabled = true;

                    }
                    else
                    {
                        Random cusid = new Random();
                        int custid = cusid.Next(1000, 9999);
                        Customers ex = _db._Customers.SingleOrDefault(cus => cus.ID == custid);
                        while (ex != null)
                        {
                            custid = cusid.Next(1000, 9999);
                            ex = _db._Customers.SingleOrDefault(cus => cus.ID == custid);
                        }

                        Customers c1 = new Customers
                        {
                            ID = custid,
                            firstname = Custfirstname.Text,
                            lastname = custlastname.Text,
                            mobile = custmobile.Text,
                            email = custemail.Text,
                        };
                        _db._Customers.Add(c1);
                        _db.SaveChanges();
                        gridview();
                        bm = this.BindingContext[custgrid.DataSource];
                        custpossition.Text = (bm.Position + 1) + "  /  " + bm.Count;
                    }
                }
           
        }

        private void newcustomer_Click(object sender, EventArgs e)
        {
            Custfirstname.Clear();
            custlastname.Clear();
            custmobile.Clear();
            custemail.Clear();
            customerimg.Image = null;
            Custfirstname.Focus();
            addcustomer.Enabled = true;
            newcustomer.Enabled = false;

        }

        private void Custfirstname_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                custlastname.Focus();
            }
        }

        private void custlastname_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                custmobile.Focus();
            }
        }

        private void custmobile_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                custemail.Focus();
            }
        }

        private void custemail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                addcustomer.Focus();
            }
        }

        private void customer_Load(object sender, EventArgs e)
        {
            if (custgrid.DataSource!=null)
            { 
                gridview();
            bm = this.BindingContext[custgrid.DataSource];
            custpossition.Text = (bm.Position + 1) + "  /  " + bm.Count;
                using (var _db = new Datacontext())
                {


                    int rowindex = custgrid.CurrentCell.RowIndex;
                    int columnindex = custgrid.CurrentCell.ColumnIndex;
                    int id = Convert.ToInt32(custgrid.Rows[rowindex].Cells[0].Value.ToString());
                    var c1 = _db._Customers.SingleOrDefault(c => c.ID == id);
                    Custfirstname.Text = c1.firstname.ToString();
                    custlastname.Text = c1.lastname.ToString();
                    custmobile.Text = c1.mobile.ToString();
                    custemail.Text = c1.email.ToString();
                    Byte[] img = (Byte[])c1.image;
                    if (img != null)
                    {
                        Stream ms = new MemoryStream(img);

                        customerimg.Image = Image.FromStream(ms);
                    }
                    else
                    {
                        customerimg.Image = null;
                    }
                    bm = this.BindingContext[custgrid.DataSource];
                    custpossition.Text = (bm.Position + 1) + "  /  " + bm.Count;
                }
            }
        }

        private void deletecustomer_Click(object sender, EventArgs e)
        {
            bm.EndCurrentEdit();
            if (MessageBox.Show("are you sure the you want to delete this Customer", "delete Customer", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
            {
                using (var _db = new Datacontext())
                {
                    int rowindex = custgrid.CurrentCell.RowIndex;
                    int columnindex = custgrid.CurrentCell.ColumnIndex;
                    int id = Convert.ToInt32(custgrid.Rows[rowindex].Cells[0].Value.ToString());
                    foreach (Customers c1 in _db._Customers)
                    {
                        if (c1.ID == id)
                        {
                            _db._Customers.Remove(c1);
                            break;
                        }
                    }
                    // Save Changes
                    MessageBox.Show("Customer has deleted", "delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _db.SaveChanges();
                    gridview();
                    bm = this.BindingContext[custgrid.DataSource];
                    custpossition.Text = (bm.Position + 1) + "  /  " + bm.Count;
                }
            }
        }

        private void updatecustomer_Click(object sender, EventArgs e)
        {
            bm.EndCurrentEdit();
            using (var _db = new Datacontext())
            {
                MemoryStream m = new MemoryStream();
               customerimg.Image.Save(m, customerimg.Image.RawFormat);
                byte[] byteimg = m.ToArray();
                int rowindex = custgrid.CurrentCell.RowIndex;
                int columnindex =custgrid.CurrentCell.ColumnIndex;
                int id = Convert.ToInt32(custgrid.Rows[rowindex].Cells[0].Value);
                var c1 = _db._Customers.SingleOrDefault(c => c.ID == id);
                c1.firstname = Custfirstname.Text;
                c1.lastname = Custfirstname.Text;
                c1.mobile = custmobile.Text;
                c1.email = custemail.Text;
                c1.image = byteimg;
                _db.SaveChanges();
                MessageBox.Show("Updated!");
                gridview();
                bm = this.BindingContext[custgrid.DataSource];
                custpossition.Text = (bm.Position + 1) + "  /  " + bm.Count;
            }
        }

        private void custgrid_Click(object sender, EventArgs e)
        {
            while (custgrid.DataSource != null)
            {
                using (var _db = new Datacontext())
                {
                    int rowindex = custgrid.CurrentCell.RowIndex;
                    int columnindex = custgrid.CurrentCell.ColumnIndex;
                    int id = Convert.ToInt32(custgrid.Rows[rowindex].Cells[0].Value.ToString());
                    var c1 = _db._Customers.SingleOrDefault(c => c.ID == id);
                    Custfirstname.Text = c1.firstname.ToString();
                    custlastname.Text = c1.lastname.ToString();
                    custmobile.Text = c1.mobile.ToString();
                    custemail.Text = c1.email.ToString();
                    Byte[] img = (Byte[])c1.image;
                    if (img != null)
                    {
                        Stream ms = new MemoryStream(img);

                        customerimg.Image = Image.FromStream(ms);
                    }
                    else
                    {
                        customerimg.Image = null;
                    }
                    bm = this.BindingContext[custgrid.DataSource];
                    custpossition.Text = (bm.Position + 1) + "  /  " + bm.Count;
                }
            }
        }

        private void firstcustomer_Click(object sender, EventArgs e)
        {
            if (bm.Position == 0)
            {
                MessageBox.Show("this is the first Customer");
            }
            bm.Position = 0;
            custpossition.Text = (bm.Position + 1) + "  /  " + bm.Count;
            using (var _db = new Datacontext())
            {
                int rowindex = custgrid.CurrentCell.RowIndex;
                int columnindex = custgrid.CurrentCell.ColumnIndex;
                int id = Convert.ToInt32(custgrid.Rows[rowindex].Cells[0].Value.ToString());
                var c1 = _db._Customers.SingleOrDefault(c => c.ID == id);
                Custfirstname.Text = c1.firstname.ToString();
                custlastname.Text = c1.lastname.ToString();
                custmobile.Text = c1.mobile.ToString();
                custemail.Text = c1.email.ToString();
                Byte[] img = (Byte[])c1.image;
                if (img != null)
                {
                    Stream ms = new MemoryStream(img);

                    customerimg.Image = Image.FromStream(ms);
                }
                else
                {
                    customerimg.Image = null;
                }
                bm = this.BindingContext[custgrid.DataSource];
                custpossition.Text = (bm.Position + 1) + "  /  " + bm.Count;
            }
        }

        private void lastcustomer_Click(object sender, EventArgs e)
        {
            if (bm.Position == bm.Count - 1)
            {
                MessageBox.Show("this is the last Customer");
            }
            bm.Position = bm.Count;
            custpossition.Text = (bm.Position + 1) + "  /  " + bm.Count;
            using (var _db = new Datacontext())
            {
                int rowindex = custgrid.CurrentCell.RowIndex;
                int columnindex = custgrid.CurrentCell.ColumnIndex;
                int id = Convert.ToInt32(custgrid.Rows[rowindex].Cells[0].Value.ToString());
                var c1 = _db._Customers.SingleOrDefault(c => c.ID == id);
                Custfirstname.Text = c1.firstname.ToString();
                custlastname.Text = c1.lastname.ToString();
                custmobile.Text = c1.mobile.ToString();
                custemail.Text = c1.email.ToString();
                Byte[] img = (Byte[])c1.image;
                if (img != null)
                {
                    Stream ms = new MemoryStream(img);

                    customerimg.Image = Image.FromStream(ms);
                }
                else
                {
                    customerimg.Image = null;
                }
                bm = this.BindingContext[custgrid.DataSource];
                custpossition.Text = (bm.Position + 1) + "  /  " + bm.Count;
            }
        }

        private void prevouscustomer_Click(object sender, EventArgs e)
        {
            if(bm.Position==0)
            {
                MessageBox.Show("this is the first Customer");
            }
            bm.Position -= 1;
            custpossition.Text = (bm.Position + 1) + "  /  " + bm.Count;
            using (var _db = new Datacontext())
            {
                int rowindex = custgrid.CurrentCell.RowIndex;
                int columnindex = custgrid.CurrentCell.ColumnIndex;
                int id = Convert.ToInt32(custgrid.Rows[rowindex].Cells[0].Value.ToString());
                var c1 = _db._Customers.SingleOrDefault(c => c.ID == id);
                Custfirstname.Text = c1.firstname.ToString();
                custlastname.Text = c1.lastname.ToString();
                custmobile.Text = c1.mobile.ToString();
                custemail.Text = c1.email.ToString();
                Byte[] img = (Byte[])c1.image;
                if (img != null)
                {
                    Stream ms = new MemoryStream(img);

                    customerimg.Image = Image.FromStream(ms);
                }
                else
                {
                    customerimg.Image = null;
                }
                bm = this.BindingContext[custgrid.DataSource];
                custpossition.Text = (bm.Position + 1) + "  /  " + bm.Count;
            }
        }

        private void nextcustomer_Click(object sender, EventArgs e)
        {
            if(bm.Position== bm.Count-1)
            {
                MessageBox.Show("this is the last Customer");
            }
            bm.Position += 1;
            custpossition.Text = (bm.Position + 1) + "  /  " + bm.Count;
            using (var _db = new Datacontext())
            {
                int rowindex = custgrid.CurrentCell.RowIndex;
                int columnindex = custgrid.CurrentCell.ColumnIndex;
                int id = Convert.ToInt32(custgrid.Rows[rowindex].Cells[0].Value.ToString());
                var c1 = _db._Customers.SingleOrDefault(c => c.ID == id);
                Custfirstname.Text = c1.firstname.ToString();
                custlastname.Text = c1.lastname.ToString();
                custmobile.Text = c1.mobile.ToString();
                custemail.Text = c1.email.ToString();
                Byte[] img = (Byte[])c1.image;
                if (img != null)
                {
                    Stream ms = new MemoryStream(img);

                    customerimg.Image = Image.FromStream(ms);
                }
                else
                {
                    customerimg.Image = null;
                }
                bm = this.BindingContext[custgrid.DataSource];
                custpossition.Text = (bm.Position + 1) + "  /  " + bm.Count;
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
               
                 
             