﻿using System;
using System.Linq;
using System.Windows.Forms;
using ClassLibrary1;
using System.IO;
using System.Drawing;
using System.Data;
using Center.RPT;

namespace Center
{
    public partial class Order : Form
    {
        private string employeename;
        DataTable table = new DataTable();
        int printbillid;




        public Order(string username)
        {
            employeename = username;
            InitializeComponent();

        }
        void clear()
        {
            prodid.Clear();
            prodname.Clear();
            prodprice.Clear();
            prodquantity.Clear();
            prodamount.Clear();
            proddisc.Clear();
            prodtotal.Clear();
        }

        void dtable()
        {
            table.Columns.Add("Product id");
            table.Columns.Add("Product Name");
            table.Columns.Add("Price");
            table.Columns.Add("Quantity");
            table.Columns.Add("Amount");
            table.Columns.Add("Discount %");
            table.Columns.Add("Total");
            billgrid.DataSource = table;

        }


        private void Order_Load(object sender, EventArgs e)
        {
            dtable();
            billgrid.RowHeadersWidth = 75;
            billgrid.Columns[0].Width = 72;
            billgrid.Columns[1].Width = 105;
            billgrid.Columns[2].Width = 63;
            billgrid.Columns[3].Width = 63;
            billgrid.Columns[4].Width = 70;
            billgrid.Columns[5].Width = 70;
            billgrid.Columns[6].Width = 78;

            using (var _db = new Datacontext())
            {
                Random billidno = new Random();
                int billid1 = billidno.Next(1, 99999);
                Bills bill = _db._Bills.SingleOrDefault(p => p.ID == billid1);
                while (bill != null)
                {
                    billid1 = billidno.Next(1, 99999);
                    bill = _db._Bills.SingleOrDefault(p => p.ID == billid1);
                }
                billboxid.Text = (billid1.ToString());
                billempl.Text = employeename;

            }
            //public int printbillid = Convert.ToInt32(billboxid.Text);
        }











        private void customersbill_Click(object sender, EventArgs e)
        {
            All_customers frm = new All_customers();
            frm.ShowDialog();
            using (var _db = new Datacontext())
            {
                if (frm.allcustomerbillgrid.DataSource != null)
                {
                    int id = Convert.ToInt32(frm.allcustomerbillgrid.CurrentRow.Cells[0].Value.ToString());
                    var c1 = _db._Customers.SingleOrDefault(c => c.ID == id);
                    custbillid.Text = (c1.ID.ToString());
                    Custfirstname.Text = c1.firstname.ToString();
                    custlastname.Text = c1.lastname.ToString();
                    custmobile.Text = c1.mobile.ToString();
                    custemail.Text = c1.email.ToString();
                    Byte[] img = (Byte[])c1.image;
                    if (img != null)
                    {
                        Stream ms = new MemoryStream(img);

                        custbillimg.Image = Image.FromStream(ms);
                    }
                    else
                    {
                        custbillimg.Image = null;
                    }

                }
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {

            All_Products frm = new All_Products();
            frm.ShowDialog();

            using (var _db = new Datacontext())
            {
                int id = Convert.ToInt32(frm.allproductbill.CurrentRow.Cells[0].Value.ToString());
                var p1 = _db._product.SingleOrDefault(p => p.Id == id);
                prodid.Text = (p1.Id.ToString());
                prodname.Text = p1.Name.ToString();
                prodprice.Text = p1.Price.ToString();
            }
            prodquantity.Focus();
        }






        private void prodquantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            char c = e.KeyChar;
            if (!char.IsDigit(c) && c != 8 && c != 13)
            {
                e.Handled = true;
                MessageBox.Show("the quantity only can be numer");

            }



        }

        private void prodquantity_KeyUp(object sender, KeyEventArgs e)
        {
            if (prodquantity.Text != "")
            {
                using (var _db = new Datacontext())
                {
                    int prodidbill = Convert.ToInt32(prodid.Text);
                    product p1 = _db._product.SingleOrDefault(p => p.Id == prodidbill);
                    int prdqun = Convert.ToInt32(prodquantity.Text);
                    if (prdqun > p1.Quntity)
                    {
                        e.Handled = true;
                        MessageBox.Show("this quantity is not avalibale");
                    }
                    else
                    {
                        double amount = Convert.ToDouble(prodprice.Text) * Convert.ToInt32(prodquantity.Text);
                        prodamount.Text = Convert.ToDouble(amount).ToString();
                        if (e.KeyCode == Keys.Enter)
                        {
                            proddisc.Focus();
                        }
                    }
                }
            }
            else if (e.KeyCode == Keys.Enter)
            {
                if (prodquantity.Text == "")
                {
                    prodquantity.Text = 1.ToString();
                    double amount = Convert.ToDouble(prodprice.Text) * Convert.ToInt32(prodquantity.Text);
                    prodamount.Text = Convert.ToDouble(amount).ToString();
                    proddisc.Focus();

                }
            }
        }



        private void proddisc_KeyPress(object sender, KeyPressEventArgs e)
        {
            char c = e.KeyChar;
            if (!char.IsDigit(c) && c != 8 && c != 13 && c != Convert.ToChar(System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator))
            {
                e.Handled = true;
                MessageBox.Show("enter the correct vlaue for Discount");
            }


        }



        private void proddisc_KeyUp(object sender, KeyEventArgs e)
        {
            if (proddisc.Text != "")
            {
                int i = 0;
                double amountprod = Convert.ToDouble(prodamount.Text);
                double disco = Convert.ToDouble(proddisc.Text);
                double total = amountprod - (amountprod * (disco / 100));
                prodtotal.Text = total.ToString();
                if (e.KeyCode == Keys.Enter)
                {
                    while (i < billgrid.Rows.Count - 1)
                    {
                        if (billgrid.Rows[i].Cells[0].Value.ToString() == prodid.Text)
                        {
                            MessageBox.Show("this product is already exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            clear();
                            allproductbillbtn.Focus();
                            return;
                        }
                        i++;

                    }

                    DataRow row = table.NewRow();
                    row[0] = prodid.Text;
                    row[1] = prodname.Text;
                    row[2] = prodprice.Text;
                    row[3] = prodquantity.Text;
                    row[4] = prodamount.Text;
                    row[5] = proddisc.Text;
                    row[6] = prodtotal.Text;
                    table.Rows.Add(row);
                    billgrid.DataSource = table;
                    clear();
                    sum.Text =
                        (from DataGridViewRow roww in billgrid.Rows
                         where roww.Cells[6].FormattedValue.ToString() != string.Empty
                         select Convert.ToDouble(roww.Cells[6].FormattedValue)).Sum().ToString();
                    allproductbillbtn.Focus();
                }

            }
            else if (e.KeyCode == Keys.Enter)
            {
                int i = 0;
                if (proddisc.Text == "")
                {
                    proddisc.Text = 0.ToString();
                    double amountprod = Convert.ToDouble(prodamount.Text);
                    double disco = Convert.ToDouble(proddisc.Text);
                    double total = amountprod - (amountprod * (disco / 100));
                    prodtotal.Text = total.ToString();
                    while (i < billgrid.Rows.Count - 1)
                    {
                        if (billgrid.Rows[i].Cells[0].Value.ToString() == prodid.Text)
                        {
                            MessageBox.Show("this product is already exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            clear();
                            allproductbillbtn.Focus();
                            return;
                        }
                        i++;

                    }
                    DataRow row = table.NewRow();
                    row[0] = prodid.Text;
                    row[1] = prodname.Text;
                    row[2] = prodprice.Text;
                    row[3] = prodquantity.Text;
                    row[4] = prodamount.Text;
                    row[5] = proddisc.Text;
                    row[6] = prodtotal.Text;
                    table.Rows.Add(row);
                    billgrid.DataSource = table;
                    clear();
                    sum.Text =
                        (from DataGridViewRow roww in billgrid.Rows
                         where roww.Cells[6].FormattedValue.ToString() != string.Empty
                         select Convert.ToDouble(roww.Cells[6].FormattedValue)).Sum().ToString();
                    allproductbillbtn.Focus();




                }

            }
        }

        private void billgrid_DoubleClick(object sender, EventArgs e)
        {
            prodid.Text = billgrid.CurrentRow.Cells[0].Value.ToString();
            prodname.Text = billgrid.CurrentRow.Cells[1].Value.ToString();
            prodprice.Text = billgrid.CurrentRow.Cells[2].Value.ToString();
            prodquantity.Text = billgrid.CurrentRow.Cells[3].Value.ToString();
            prodamount.Text = billgrid.CurrentRow.Cells[4].Value.ToString();
            proddisc.Text = billgrid.CurrentRow.Cells[5].Value.ToString();
            prodtotal.Text = billgrid.CurrentRow.Cells[6].Value.ToString();
            billgrid.Rows.RemoveAt(billgrid.CurrentRow.Index);
            prodquantity.Focus();

        }

        private void billgrid_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            sum.Text =
                       (from DataGridViewRow roww in billgrid.Rows
                        where roww.Cells[6].FormattedValue.ToString() != string.Empty
                        select Convert.ToDouble(roww.Cells[6].FormattedValue)).Sum().ToString();
        }

        private void saveBill_Click(object sender, EventArgs e)
        {
            if (custbillid.Text != string.Empty)
            {
                using (var _db = new Datacontext())
                {
                    printbillid = Convert.ToInt32(billboxid.Text);
                    Bills bill = _db._Bills.SingleOrDefault(p => p.ID == printbillid);
                    while (bill != null)
                    {
                        bill = _db._Bills.SingleOrDefault(p => p.ID == printbillid);
                    }
                    Bills b1 = new Bills
                    {
                        ID = Convert.ToInt32(billboxid.Text),
                        date = datebill.Value,
                        Customer_ID = Convert.ToInt32(custbillid.Text),
                        employee_name = billempl.Text
                    };
                    _db._Bills.Add(b1);
                    for (int i = 0; i < billgrid.Rows.Count - 1; i++)
                    {
                        Random emid = new Random();
                    Start:
                        int emid1 = emid.Next(1000, 9999);
                        Orders ex = _db._Orders.SingleOrDefault(p => p.order_Id == emid1);
                        while (ex != null)
                        {
                            goto Start;
                        }
                        Orders or = new Orders
                        {
                            order_Id = emid1,
                            Product_ID = Convert.ToInt32(billgrid.Rows[i].Cells[0].Value),
                            productname = billgrid.Rows[i].Cells[1].Value.ToString(),
                            Price = Convert.ToDouble(billgrid.Rows[i].Cells[2].Value),
                            Quantity = Convert.ToInt32(billgrid.Rows[i].Cells[3].Value),
                            amount = Convert.ToDouble(billgrid.Rows[i].Cells[4].Value),
                            discount = Convert.ToDouble(billgrid.Rows[i].Cells[5].Value),
                            total_amount = Convert.ToDouble(billgrid.Rows[i].Cells[6].Value),
                            Bill_ID = Convert.ToInt32(billboxid.Text)
                        };
                        _db._Orders.Add(or);
                        _db.SaveChanges();
                        int prodidbill = Convert.ToInt32(billgrid.Rows[i].Cells[0].Value);
                        int prdqun = Convert.ToInt32(billgrid.Rows[i].Cells[3].Value);
                        product p1 = _db._product.SingleOrDefault(p => p.Id == prodidbill);
                        p1.Quntity = (p1.Quntity - prdqun);
                        foreach (product pro in _db._product)
                        {
                            if (pro.Quntity == 0)
                            {
                                _db._product.Remove(pro);
                                break;
                            }
                        }
                    }
                    _db.SaveChanges();
                }
            }
            else
            {
                using (var _db = new Datacontext())
                {
                    printbillid = Convert.ToInt32(billboxid.Text);
                    Bills bill = _db._Bills.SingleOrDefault(p => p.ID == printbillid);
                    while (bill != null)
                    {
                        bill = _db._Bills.SingleOrDefault(p => p.ID == printbillid);
                    }
                    Bills b1 = new Bills
                    {
                        ID = Convert.ToInt32(billboxid.Text),
                        date = datebill.Value,
                        employee_name = billempl.Text
                    };
                    _db._Bills.Add(b1);
                    for (int i = 0; i < billgrid.Rows.Count - 1; i++)
                    {
                        Random emid = new Random();
                    Start:
                        int emid1 = emid.Next(1000, 9999);
                        Orders ex = _db._Orders.SingleOrDefault(p => p.order_Id == emid1);
                        while (ex != null)
                        {
                            goto Start;
                        }
                        Orders or = new Orders
                        {
                            order_Id = emid1,
                            Product_ID = Convert.ToInt32(billgrid.Rows[i].Cells[0].Value),
                            productname = billgrid.Rows[i].Cells[1].Value.ToString(),
                            Price = Convert.ToDouble(billgrid.Rows[i].Cells[2].Value),
                            Quantity = Convert.ToInt32(billgrid.Rows[i].Cells[3].Value),
                            amount = Convert.ToDouble(billgrid.Rows[i].Cells[4].Value),
                            discount = Convert.ToDouble(billgrid.Rows[i].Cells[5].Value),
                            total_amount = Convert.ToDouble(billgrid.Rows[i].Cells[6].Value),
                            Bill_ID = Convert.ToInt32(billboxid.Text)
                        };
                        _db._Orders.Add(or);
                        _db.SaveChanges();
                        int prodidbill = Convert.ToInt32(billgrid.Rows[i].Cells[0].Value);
                        int prdqun = Convert.ToInt32(billgrid.Rows[i].Cells[3].Value);
                        product p1 = _db._product.SingleOrDefault(p => p.Id == prodidbill);
                        p1.Quntity = (p1.Quntity - prdqun);
                        foreach (product pro in _db._product)
                        {
                            if (pro.Quntity == 0)
                            {
                                _db._product.Remove(pro);
                                break;
                            }
                        }
                    }
                    _db.SaveChanges();
                }
            }
        }

        private void print_bill_Click(object sender, EventArgs e)
        {
            if (custbillid.Text == string.Empty)
            {
                RPT.normalbill myproreport = new RPT.normalbill();
                myproreport.SetParameterValue("@bill_id", printbillid);
                RPT.proreport frmreport = new RPT.proreport();
                frmreport.crystalReportViewer1.ReportSource = myproreport;
                frmreport.ShowDialog();
            }
            else
            {
                RPT.customerbill myproreport = new RPT.customerbill();
                myproreport.SetParameterValue("@bill_id", printbillid);
                RPT.proreport frmreport = new RPT.proreport();
                frmreport.crystalReportViewer1.ReportSource = myproreport;
                frmreport.ShowDialog();
            }
        }
    }
}
           