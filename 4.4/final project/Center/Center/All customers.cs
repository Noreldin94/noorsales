﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Center
{
    public partial class All_customers : Form
    {
        public All_customers()
        {
            InitializeComponent();
        }

        private void All_customers_Load(object sender, EventArgs e)
        {
            using (var _db = new Datacontext())
            {
                var data = _db._Customers
                    .Select(x => new { x.ID, x.firstname, x.lastname, x.mobile, x.email });
                allcustomerbillgrid.DataSource = data.ToList();
            }
        }

        private void allcustomerbillgrid_DoubleClick(object sender, EventArgs e)
        {
            this.Close();
            
            
                
            
        }
            
    }
}
