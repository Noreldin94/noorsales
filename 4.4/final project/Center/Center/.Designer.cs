﻿namespace Center
{
    partial class categories
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(categories));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.categPoss = new System.Windows.Forms.Label();
            this.last = new System.Windows.Forms.Button();
            this.next = new System.Windows.Forms.Button();
            this.prevous = new System.Windows.Forms.Button();
            this.first = new System.Windows.Forms.Button();
            this.textdes = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.deletecateg = new System.Windows.Forms.Button();
            this.categadd = new System.Windows.Forms.Button();
            this.categnew = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.categgrid = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.categgrid)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.categPoss);
            this.groupBox1.Controls.Add(this.last);
            this.groupBox1.Controls.Add(this.next);
            this.groupBox1.Controls.Add(this.prevous);
            this.groupBox1.Controls.Add(this.first);
            this.groupBox1.Controls.Add(this.textdes);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(386, 183);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // categPoss
            // 
            this.categPoss.AutoSize = true;
            this.categPoss.Location = new System.Drawing.Point(174, 141);
            this.categPoss.Name = "categPoss";
            this.categPoss.Size = new System.Drawing.Size(35, 13);
            this.categPoss.TabIndex = 6;
            this.categPoss.Text = "label2";
            this.categPoss.Click += new System.EventHandler(this.categPoss_Click);
            // 
            // last
            // 
            this.last.Location = new System.Drawing.Point(297, 136);
            this.last.Name = "last";
            this.last.Size = new System.Drawing.Size(75, 23);
            this.last.TabIndex = 5;
            this.last.Text = ">>";
            this.last.UseVisualStyleBackColor = true;
            this.last.Click += new System.EventHandler(this.last_Click);
            // 
            // next
            // 
            this.next.Location = new System.Drawing.Point(228, 136);
            this.next.Name = "next";
            this.next.Size = new System.Drawing.Size(75, 23);
            this.next.TabIndex = 4;
            this.next.Text = ">";
            this.next.UseVisualStyleBackColor = true;
            this.next.Click += new System.EventHandler(this.next_Click);
            // 
            // prevous
            // 
            this.prevous.Location = new System.Drawing.Point(80, 136);
            this.prevous.Name = "prevous";
            this.prevous.Size = new System.Drawing.Size(75, 23);
            this.prevous.TabIndex = 3;
            this.prevous.Text = "<";
            this.prevous.UseVisualStyleBackColor = true;
            this.prevous.Click += new System.EventHandler(this.prevous_Click);
            // 
            // first
            // 
            this.first.Location = new System.Drawing.Point(6, 136);
            this.first.Name = "first";
            this.first.Size = new System.Drawing.Size(75, 23);
            this.first.TabIndex = 2;
            this.first.Text = "<<";
            this.first.UseVisualStyleBackColor = true;
            this.first.Click += new System.EventHandler(this.first_Click);
            // 
            // textdes
            // 
            this.textdes.Location = new System.Drawing.Point(80, 50);
            this.textdes.Multiline = true;
            this.textdes.Name = "textdes";
            this.textdes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textdes.Size = new System.Drawing.Size(292, 66);
            this.textdes.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Type";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.button13);
            this.groupBox2.Controls.Add(this.button12);
            this.groupBox2.Controls.Add(this.button11);
            this.groupBox2.Controls.Add(this.button10);
            this.groupBox2.Controls.Add(this.button9);
            this.groupBox2.Controls.Add(this.button8);
            this.groupBox2.Controls.Add(this.deletecateg);
            this.groupBox2.Controls.Add(this.categadd);
            this.groupBox2.Controls.Add(this.categnew);
            this.groupBox2.Location = new System.Drawing.Point(12, 201);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(708, 76);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(297, 47);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(114, 23);
            this.button13.TabIndex = 8;
            this.button13.Text = "Close";
            this.button13.UseVisualStyleBackColor = true;
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(573, 19);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(114, 23);
            this.button12.TabIndex = 7;
            this.button12.Text = "Save All as pdf";
            this.button12.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(492, 19);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 23);
            this.button11.TabIndex = 6;
            this.button11.Text = "Save as pdf";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(411, 19);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 5;
            this.button10.Text = "Print All";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(330, 19);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 4;
            this.button9.Text = "Print";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(249, 19);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 3;
            this.button8.Text = "Update";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // deletecateg
            // 
            this.deletecateg.Location = new System.Drawing.Point(168, 19);
            this.deletecateg.Name = "deletecateg";
            this.deletecateg.Size = new System.Drawing.Size(75, 23);
            this.deletecateg.TabIndex = 2;
            this.deletecateg.Text = "Delete";
            this.deletecateg.UseVisualStyleBackColor = true;
            this.deletecateg.Click += new System.EventHandler(this.deletecateg_Click);
            // 
            // categadd
            // 
            this.categadd.Location = new System.Drawing.Point(87, 19);
            this.categadd.Name = "categadd";
            this.categadd.Size = new System.Drawing.Size(75, 23);
            this.categadd.TabIndex = 1;
            this.categadd.Text = "Add";
            this.categadd.UseVisualStyleBackColor = true;
            this.categadd.Click += new System.EventHandler(this.categadd_Click);
            // 
            // categnew
            // 
            this.categnew.Location = new System.Drawing.Point(6, 19);
            this.categnew.Name = "categnew";
            this.categnew.Size = new System.Drawing.Size(75, 23);
            this.categnew.TabIndex = 0;
            this.categnew.Text = "New";
            this.categnew.UseVisualStyleBackColor = true;
            this.categnew.Click += new System.EventHandler(this.categnew_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.categgrid);
            this.groupBox3.Location = new System.Drawing.Point(404, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(344, 183);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // categgrid
            // 
            this.categgrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.categgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.categgrid.Location = new System.Drawing.Point(6, 19);
            this.categgrid.Name = "categgrid";
            this.categgrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.categgrid.Size = new System.Drawing.Size(332, 158);
            this.categgrid.TabIndex = 0;
            this.categgrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.categgrid_CellClick);
            // 
            // categories
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(750, 350);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "categories";
            this.Text = "categories";
            this.Load += new System.EventHandler(this.categories_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.categgrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button last;
        private System.Windows.Forms.Button next;
        private System.Windows.Forms.Button prevous;
        private System.Windows.Forms.Button first;
        private System.Windows.Forms.TextBox textdes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button deletecateg;
        private System.Windows.Forms.Button categadd;
        private System.Windows.Forms.Button categnew;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView categgrid;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label categPoss;
    }
}