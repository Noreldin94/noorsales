﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary1;
using System.IO;
using System.Windows.Forms;
using System.Drawing.Imaging;
using CrystalDecisions.Shared;

namespace Center
{
    public partial class Products : Form
    {
        public Products()
        {
            InitializeComponent();
        }

        private void Products_Load(object sender, EventArgs e)
        {
            using (var _db = new Datacontext())
            {
                combopro.DataSource = _db._Categories.ToList();
                combopro.DisplayMember = "Type";
                combopro.ValueMember = "Id";
                var pro = _db._product.ToList();
                progrid.DataSource = pro;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (var _db = new Datacontext())
            {
                MemoryStream m = new MemoryStream();
                proimg.Image.Save(m, proimg.Image.RawFormat);
                byte[] byteimg = m.ToArray();
                Random prid = new Random();
                int prid1 = prid.Next(1000, 9999);
                product ex = _db._product.SingleOrDefault(pr => pr.Id == prid1);
                while (ex != null)
                {
                    prid1 = prid.Next(1000, 9999);
                    ex = _db._product.SingleOrDefault(pr => pr.Id == prid1);
                }
                int id = (from s in _db._Categories
                          where s.Type.Equals(combopro.Text.ToString())
                          select s.ID).First();
                
                    product p1 = new product
                    {
                        Id = prid1,
                        Name = proname.Text,
                        Quntity =Convert.ToInt32( proquantity.Text),
                        Price =Convert.ToDouble( proprice.Text),
                        Type = combopro.Text,
                        image = byteimg,
                        Description = prodesc.Text,
                        categ_Id = id





                    };
                    _db._product.Add(p1);
                    _db.SaveChanges();
                    progrid.DataSource = _db._product.ToList();


                }

            
        }

        private void combopro_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)

        {

            using (var _db = new Datacontext())
            {
                RPT.selectedproduct myproreport = new RPT.selectedproduct();
                myproreport.SetParameterValue("@Id", this.progrid.CurrentRow.Cells[0].Value.ToString());
                RPT.proreport frmreport = new RPT.proreport();
                frmreport.crystalReportViewer1.ReportSource = myproreport;
                frmreport.ShowDialog();

            }



        }





        private void button9_Click(object sender, EventArgs e)
        {
            using (var _db = new Datacontext())
            {

                if (pronamesearch.Text != "")
                {
                    var search = from mn in _db._product
                                 where mn.Name.Equals(pronamesearch.Text)
                                 select mn;

                    progrid.DataSource = search.ToList();
                }

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("are you sure the you want to delete this item", "delete item", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
            {
                using (var _db = new Datacontext())
                {
                    int rowindex = progrid.CurrentCell.RowIndex;
                    int columnindex = progrid.CurrentCell.ColumnIndex;
                    int id = Convert.ToInt32(progrid.Rows[rowindex].Cells[0].Value.ToString());
                    foreach (product pro in _db._product)
                    {
                        if (pro.Id == id)
                        {
                            _db._product.Remove(pro);
                            break;
                        }
                    }
                    // Save Changes
                    MessageBox.Show("item has deleted", "delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _db.SaveChanges();
                    progrid.DataSource = _db._product.ToList();

                }

            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            RPT.allproduct myproreport = new RPT.allproduct();
            RPT.proreport frmreport = new RPT.proreport();
            frmreport.crystalReportViewer1.ReportSource = myproreport;
            frmreport.ShowDialog();

        }

        private void button8_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            RPT.allproduct myproreport = new RPT.allproduct();
            ExportOptions exp = new ExportOptions();
            DiskFileDestinationOptions dfop = new DiskFileDestinationOptions();
            ExcelFormatOptions excel = new ExcelFormatOptions();
            dfop.DiskFileName = @"E:\allproduct.xls";
            exp = myproreport.ExportOptions;
            exp.ExportDestinationType = ExportDestinationType.DiskFile;
            exp.ExportFormatType = ExportFormatType.Excel;
            exp.ExportFormatOptions = excel;
            exp.ExportDestinationOptions = dfop;
            myproreport.Export();
            MessageBox.Show("saved", "save file", MessageBoxButtons.OK);




        }

        private void probrowimg_Click(object sender, EventArgs e)
        {
            OpenFileDialog opf = new OpenFileDialog();
            opf.Filter = "image files|*.jpg;*.png;*.gif;*.icon";
            DialogResult dr = opf.ShowDialog();


            if (dr == DialogResult.Cancel)
                return;
            proimg.Image = Image.FromFile(opf.FileName);
            proimg.SizeMode = PictureBoxSizeMode.StretchImage;
            proimg.BorderStyle = BorderStyle.Fixed3D;
        }

        private void progrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowindex = progrid.CurrentCell.RowIndex;
            int columnindex = progrid.CurrentCell.ColumnIndex;
            proname.Text = progrid.Rows[rowindex].Cells[2].Value.ToString();
            proquantity.Text = progrid.Rows[rowindex].Cells[3].Value.ToString();
            proprice.Text = progrid.Rows[rowindex].Cells[4].Value.ToString();
            combopro.Text = progrid.Rows[rowindex].Cells[1].Value.ToString();
            prodesc.Text = progrid.Rows[rowindex].Cells[5].Value.ToString();
            Byte[] img = (Byte[])progrid.Rows[rowindex].Cells[6].Value;
            Stream ms = new MemoryStream(img);
            proimg.Image = Image.FromStream(ms);

        }

        private void button4_Click(object sender, EventArgs e)
        {
            int rowindex = progrid.CurrentCell.RowIndex;
            int columnindex = progrid.CurrentCell.ColumnIndex;
            prophoto frm = new prophoto();
            Byte[] img = (Byte[])progrid.Rows[rowindex].Cells[6].Value;
            Stream ms = new MemoryStream(img);
            frm.prophot.Image = Image.FromStream(ms);
            frm.ShowDialog();


        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (var _db = new Datacontext())
            {
                MemoryStream m = new MemoryStream();
                proimg.Image.Save(m, proimg.Image.RawFormat);
                byte[] byteimg = m.ToArray();
                int rowindex = progrid.CurrentCell.RowIndex;
                int columnindex = progrid.CurrentCell.ColumnIndex;
                int ID = Convert.ToInt32(progrid.Rows[rowindex].Cells[0].Value);
                var pro = _db._product.SingleOrDefault(p => p.Id == ID);
                pro.Name = proname.Text;
                pro.Quntity =Convert.ToInt32( proquantity.Text);
                pro.Price =Convert.ToDouble( proprice.Text);
                pro.Type = combopro.SelectedItem.ToString();
                pro.image = byteimg;
                pro.Description = prodesc.Text;
                _db.SaveChanges();
                MessageBox.Show("Updated!");
                progrid.DataSource = _db._product.ToList();
            }
        }
    }
}
                   
   
