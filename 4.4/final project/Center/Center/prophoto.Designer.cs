﻿namespace Center
{
    partial class prophoto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.prophot = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.prophot)).BeginInit();
            this.SuspendLayout();
            // 
            // prophot
            // 
            this.prophot.Location = new System.Drawing.Point(-5, 1);
            this.prophot.Name = "prophot";
            this.prophot.Size = new System.Drawing.Size(498, 368);
            this.prophot.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.prophot.TabIndex = 0;
            this.prophot.TabStop = false;
            // 
            // prophoto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 364);
            this.Controls.Add(this.prophot);
            this.Name = "prophoto";
            this.Text = "prophoto";
            ((System.ComponentModel.ISupportInitialize)(this.prophot)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.PictureBox prophot;
    }
}