﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClassLibrary1
{
    public class Orders
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)] 
        [Key]
        public int order_Id { get; set; }
        public int Product_ID { get; set; }
        //[ForeignKey("Product_ID")]
        //public product products { get; set; }
        public int Bill_ID { get; set; }
        //[ForeignKey("Bill_ID")]
        //public Bills Bills { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public double discount { get; set; }
        public double amount { get; set; }
        public double total_amount { get; set; }
        public string productname { get; set; }
      
       

       
    }
}
