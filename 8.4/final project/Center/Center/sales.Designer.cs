﻿namespace Center
{
    partial class sales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.salesgrid = new System.Windows.Forms.DataGridView();
            this.sum = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.searchsales = new System.Windows.Forms.Button();
            this.fisrstdateorders = new System.Windows.Forms.DateTimePicker();
            this.lastdateorders = new System.Windows.Forms.DateTimePicker();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.salesgrid)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.salesgrid);
            this.groupBox1.Location = new System.Drawing.Point(226, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(597, 396);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // salesgrid
            // 
            this.salesgrid.AllowUserToAddRows = false;
            this.salesgrid.AllowUserToDeleteRows = false;
            this.salesgrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.salesgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.salesgrid.Location = new System.Drawing.Point(9, 18);
            this.salesgrid.Name = "salesgrid";
            this.salesgrid.ReadOnly = true;
            this.salesgrid.Size = new System.Drawing.Size(582, 372);
            this.salesgrid.TabIndex = 0;
            // 
            // sum
            // 
            this.sum.BackColor = System.Drawing.Color.Transparent;
            this.sum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sum.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.sum.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sum.ForeColor = System.Drawing.Color.DimGray;
            this.sum.Location = new System.Drawing.Point(408, 402);
            this.sum.Name = "sum";
            this.sum.Size = new System.Drawing.Size(204, 66);
            this.sum.TabIndex = 46;
            this.sum.Text = "0";
            this.sum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.searchsales);
            this.groupBox3.Controls.Add(this.fisrstdateorders);
            this.groupBox3.Controls.Add(this.lastdateorders);
            this.groupBox3.Location = new System.Drawing.Point(3, 44);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(217, 304);
            this.groupBox3.TabIndex = 47;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // searchsales
            // 
            this.searchsales.Location = new System.Drawing.Point(52, 248);
            this.searchsales.Name = "searchsales";
            this.searchsales.Size = new System.Drawing.Size(75, 23);
            this.searchsales.TabIndex = 4;
            this.searchsales.Text = "search";
            this.searchsales.UseVisualStyleBackColor = true;
            this.searchsales.Click += new System.EventHandler(this.searchsales_Click);
            // 
            // fisrstdateorders
            // 
            this.fisrstdateorders.Location = new System.Drawing.Point(6, 87);
            this.fisrstdateorders.Name = "fisrstdateorders";
            this.fisrstdateorders.Size = new System.Drawing.Size(200, 20);
            this.fisrstdateorders.TabIndex = 2;
            // 
            // lastdateorders
            // 
            this.lastdateorders.Location = new System.Drawing.Point(6, 171);
            this.lastdateorders.Name = "lastdateorders";
            this.lastdateorders.Size = new System.Drawing.Size(200, 20);
            this.lastdateorders.TabIndex = 3;
            // 
            // sales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(853, 474);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.sum);
            this.Controls.Add(this.groupBox1);
            this.Name = "sales";
            this.Text = "sales";
            this.Load += new System.EventHandler(this.sales_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.salesgrid)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView salesgrid;
        private System.Windows.Forms.Label sum;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button searchsales;
        private System.Windows.Forms.DateTimePicker fisrstdateorders;
        private System.Windows.Forms.DateTimePicker lastdateorders;
    }
}