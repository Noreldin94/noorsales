﻿namespace Center
{
    partial class roles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Employeecheck = new System.Windows.Forms.CheckBox();
            this.billcheck = new System.Windows.Forms.CheckBox();
            this.orderscheck = new System.Windows.Forms.CheckBox();
            this.rolescombo = new System.Windows.Forms.ComboBox();
            this.saveroles = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Employeecheck
            // 
            this.Employeecheck.AutoSize = true;
            this.Employeecheck.Location = new System.Drawing.Point(193, 42);
            this.Employeecheck.Name = "Employeecheck";
            this.Employeecheck.Size = new System.Drawing.Size(66, 17);
            this.Employeecheck.TabIndex = 0;
            this.Employeecheck.Text = "emplyee";
            this.Employeecheck.UseVisualStyleBackColor = true;
            // 
            // billcheck
            // 
            this.billcheck.AutoSize = true;
            this.billcheck.Location = new System.Drawing.Point(193, 94);
            this.billcheck.Name = "billcheck";
            this.billcheck.Size = new System.Drawing.Size(38, 17);
            this.billcheck.TabIndex = 1;
            this.billcheck.Text = "bill";
            this.billcheck.UseVisualStyleBackColor = true;
            // 
            // orderscheck
            // 
            this.orderscheck.AutoSize = true;
            this.orderscheck.Location = new System.Drawing.Point(193, 152);
            this.orderscheck.Name = "orderscheck";
            this.orderscheck.Size = new System.Drawing.Size(57, 17);
            this.orderscheck.TabIndex = 2;
            this.orderscheck.Text = "orders";
            this.orderscheck.UseVisualStyleBackColor = true;
            // 
            // rolescombo
            // 
            this.rolescombo.FormattingEnabled = true;
            this.rolescombo.Location = new System.Drawing.Point(12, 92);
            this.rolescombo.Name = "rolescombo";
            this.rolescombo.Size = new System.Drawing.Size(121, 21);
            this.rolescombo.TabIndex = 3;
            // 
            // saveroles
            // 
            this.saveroles.Location = new System.Drawing.Point(49, 152);
            this.saveroles.Name = "saveroles";
            this.saveroles.Size = new System.Drawing.Size(75, 23);
            this.saveroles.TabIndex = 4;
            this.saveroles.Text = "button1";
            this.saveroles.UseVisualStyleBackColor = true;
            this.saveroles.Click += new System.EventHandler(this.saveroles_Click);
            // 
            // roles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.saveroles);
            this.Controls.Add(this.rolescombo);
            this.Controls.Add(this.orderscheck);
            this.Controls.Add(this.billcheck);
            this.Controls.Add(this.Employeecheck);
            this.Name = "roles";
            this.Text = "roles";
            this.Load += new System.EventHandler(this.roles_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox Employeecheck;
        private System.Windows.Forms.CheckBox billcheck;
        private System.Windows.Forms.CheckBox orderscheck;
        private System.Windows.Forms.ComboBox rolescombo;
        private System.Windows.Forms.Button saveroles;
    }
}