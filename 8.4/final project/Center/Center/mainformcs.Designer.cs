﻿namespace Center
{
    partial class mainformcs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainformcs));
            this.newem = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.ordersform = new System.Windows.Forms.Button();
            this.Billsform = new System.Windows.Forms.Button();
            this.salesform = new System.Windows.Forms.Button();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mainfirmemppic = new System.Windows.Forms.PictureBox();
            this.mainfirmempname = new System.Windows.Forms.Label();
            this.roles = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainfirmemppic)).BeginInit();
            this.SuspendLayout();
            // 
            // newem
            // 
            this.newem.Enabled = false;
            this.newem.Location = new System.Drawing.Point(153, 12);
            this.newem.Name = "newem";
            this.newem.Size = new System.Drawing.Size(119, 57);
            this.newem.TabIndex = 0;
            this.newem.Text = "New Employee";
            this.newem.UseVisualStyleBackColor = true;
            this.newem.Click += new System.EventHandler(this.newem_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(153, 89);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(119, 51);
            this.button1.TabIndex = 1;
            this.button1.Text = "Products";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(153, 162);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(119, 49);
            this.button2.TabIndex = 2;
            this.button2.Text = "categories";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(153, 230);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(119, 49);
            this.button3.TabIndex = 3;
            this.button3.Text = "Customers";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // ordersform
            // 
            this.ordersform.Enabled = false;
            this.ordersform.Location = new System.Drawing.Point(153, 302);
            this.ordersform.Name = "ordersform";
            this.ordersform.Size = new System.Drawing.Size(119, 49);
            this.ordersform.TabIndex = 4;
            this.ordersform.Text = "Orders";
            this.ordersform.UseVisualStyleBackColor = true;
            this.ordersform.Click += new System.EventHandler(this.button4_Click);
            // 
            // Billsform
            // 
            this.Billsform.Enabled = false;
            this.Billsform.Location = new System.Drawing.Point(12, 12);
            this.Billsform.Name = "Billsform";
            this.Billsform.Size = new System.Drawing.Size(119, 57);
            this.Billsform.TabIndex = 5;
            this.Billsform.Text = "Bills";
            this.Billsform.UseVisualStyleBackColor = true;
            this.Billsform.Click += new System.EventHandler(this.Billsform_Click);
            // 
            // salesform
            // 
            this.salesform.Location = new System.Drawing.Point(12, 83);
            this.salesform.Name = "salesform";
            this.salesform.Size = new System.Drawing.Size(119, 57);
            this.salesform.TabIndex = 6;
            this.salesform.Text = "Sales";
            this.salesform.UseVisualStyleBackColor = true;
            this.salesform.Click += new System.EventHandler(this.salesform_Click);
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.monthCalendar1.Location = new System.Drawing.Point(669, 207);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 7;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.mainfirmemppic);
            this.groupBox1.Controls.Add(this.mainfirmempname);
            this.groupBox1.Location = new System.Drawing.Point(501, 63);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(258, 126);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // mainfirmemppic
            // 
            this.mainfirmemppic.Location = new System.Drawing.Point(36, 19);
            this.mainfirmemppic.Name = "mainfirmemppic";
            this.mainfirmemppic.Size = new System.Drawing.Size(100, 101);
            this.mainfirmemppic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.mainfirmemppic.TabIndex = 1;
            this.mainfirmemppic.TabStop = false;
            // 
            // mainfirmempname
            // 
            this.mainfirmempname.AutoSize = true;
            this.mainfirmempname.Location = new System.Drawing.Point(156, 26);
            this.mainfirmempname.Name = "mainfirmempname";
            this.mainfirmempname.Size = new System.Drawing.Size(35, 13);
            this.mainfirmempname.TabIndex = 0;
            this.mainfirmempname.Text = "label1";
            // 
            // roles
            // 
            this.roles.Location = new System.Drawing.Point(436, 261);
            this.roles.Name = "roles";
            this.roles.Size = new System.Drawing.Size(128, 66);
            this.roles.TabIndex = 9;
            this.roles.Text = "roles";
            this.roles.UseVisualStyleBackColor = true;
            this.roles.Click += new System.EventHandler(this.roles_Click);
            // 
            // mainformcs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(904, 456);
            this.Controls.Add(this.roles);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.salesform);
            this.Controls.Add(this.Billsform);
            this.Controls.Add(this.ordersform);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.newem);
            this.Name = "mainformcs";
            this.Text = "mainformcs";
            this.Load += new System.EventHandler(this.mainformcs_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainfirmemppic)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button newem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button ordersform;
        private System.Windows.Forms.Button Billsform;
        private System.Windows.Forms.Button salesform;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox mainfirmemppic;
        private System.Windows.Forms.Label mainfirmempname;
        private System.Windows.Forms.Button roles;
    }
}