﻿namespace Center
{
    partial class register
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(register));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.empname = new System.Windows.Forms.TextBox();
            this.empemail = new System.Windows.Forms.TextBox();
            this.emppassword = new System.Windows.Forms.TextBox();
            this.empidnum = new System.Windows.Forms.TextBox();
            this.emprole = new System.Windows.Forms.ComboBox();
            this.Addemployee = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.empimage = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.empgrid = new System.Windows.Forms.DataGridView();
            this.button3 = new System.Windows.Forms.Button();
            this.Updateempinfo = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.empgender = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.empmobile = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.empimage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.empgrid)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(116, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(391, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "Email";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(660, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 19);
            this.label3.TabIndex = 2;
            this.label3.Text = "password";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(98, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 19);
            this.label4.TabIndex = 3;
            this.label4.Text = "ID number";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(391, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 19);
            this.label5.TabIndex = 4;
            this.label5.Text = "Role";
            // 
            // empname
            // 
            this.empname.Location = new System.Drawing.Point(197, 9);
            this.empname.Name = "empname";
            this.empname.Size = new System.Drawing.Size(152, 20);
            this.empname.TabIndex = 0;
            // 
            // empemail
            // 
            this.empemail.Location = new System.Drawing.Point(467, 8);
            this.empemail.Name = "empemail";
            this.empemail.Size = new System.Drawing.Size(152, 20);
            this.empemail.TabIndex = 1;
            this.empemail.TextChanged += new System.EventHandler(this.empemail_TextChanged);
            // 
            // emppassword
            // 
            this.emppassword.Location = new System.Drawing.Point(762, 8);
            this.emppassword.Name = "emppassword";
            this.emppassword.Size = new System.Drawing.Size(152, 20);
            this.emppassword.TabIndex = 3;
            // 
            // empidnum
            // 
            this.empidnum.Location = new System.Drawing.Point(197, 46);
            this.empidnum.Name = "empidnum";
            this.empidnum.Size = new System.Drawing.Size(152, 20);
            this.empidnum.TabIndex = 4;
            // 
            // emprole
            // 
            this.emprole.FormattingEnabled = true;
            this.emprole.Items.AddRange(new object[] {
            "Admin",
            "Normal"});
            this.emprole.Location = new System.Drawing.Point(467, 45);
            this.emprole.Name = "emprole";
            this.emprole.Size = new System.Drawing.Size(152, 21);
            this.emprole.TabIndex = 6;
            // 
            // Addemployee
            // 
            this.Addemployee.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Addemployee.Location = new System.Drawing.Point(37, 190);
            this.Addemployee.Name = "Addemployee";
            this.Addemployee.Size = new System.Drawing.Size(99, 39);
            this.Addemployee.TabIndex = 10;
            this.Addemployee.Text = "Add";
            this.Addemployee.UseVisualStyleBackColor = true;
            this.Addemployee.Click += new System.EventHandler(this.Addnem_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(645, 108);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 45);
            this.button1.TabIndex = 9;
            this.button1.Text = "Browse";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // empimage
            // 
            this.empimage.Location = new System.Drawing.Point(467, 82);
            this.empimage.Name = "empimage";
            this.empimage.Size = new System.Drawing.Size(152, 106);
            this.empimage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.empimage.TabIndex = 12;
            this.empimage.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(391, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 19);
            this.label6.TabIndex = 13;
            this.label6.Text = "Image";
            // 
            // empgrid
            // 
            this.empgrid.AllowUserToAddRows = false;
            this.empgrid.AllowUserToDeleteRows = false;
            this.empgrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 8F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.empgrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.empgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.empgrid.Location = new System.Drawing.Point(1, 247);
            this.empgrid.MultiSelect = false;
            this.empgrid.Name = "empgrid";
            this.empgrid.ReadOnly = true;
            this.empgrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.empgrid.Size = new System.Drawing.Size(933, 251);
            this.empgrid.TabIndex = 14;
            this.empgrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.empgrid_CellClick);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(581, 190);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(92, 39);
            this.button3.TabIndex = 13;
            this.button3.Text = "Delete";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Updateempinfo
            // 
            this.Updateempinfo.Location = new System.Drawing.Point(395, 189);
            this.Updateempinfo.Name = "Updateempinfo";
            this.Updateempinfo.Size = new System.Drawing.Size(105, 40);
            this.Updateempinfo.TabIndex = 12;
            this.Updateempinfo.Text = "Update";
            this.Updateempinfo.UseVisualStyleBackColor = true;
            this.Updateempinfo.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(222, 188);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(112, 41);
            this.button5.TabIndex = 11;
            this.button5.Text = "search";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // empgender
            // 
            this.empgender.FormattingEnabled = true;
            this.empgender.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.empgender.Location = new System.Drawing.Point(762, 45);
            this.empgender.Name = "empgender";
            this.empgender.Size = new System.Drawing.Size(152, 21);
            this.empgender.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(660, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 19);
            this.label7.TabIndex = 20;
            this.label7.Text = "Gender";
            // 
            // empmobile
            // 
            this.empmobile.Location = new System.Drawing.Point(197, 82);
            this.empmobile.Name = "empmobile";
            this.empmobile.Size = new System.Drawing.Size(152, 20);
            this.empmobile.TabIndex = 8;
            this.empmobile.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.empmobile_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(116, 82);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 19);
            this.label8.TabIndex = 22;
            this.label8.Text = "Mobile";
            // 
            // register
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(935, 500);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.empmobile);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.empgender);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.Updateempinfo);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.empgrid);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.empimage);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Addemployee);
            this.Controls.Add(this.emprole);
            this.Controls.Add(this.empidnum);
            this.Controls.Add(this.emppassword);
            this.Controls.Add(this.empemail);
            this.Controls.Add(this.empname);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "register";
            this.Text = "zz";
            this.Load += new System.EventHandler(this.register_Load);
            ((System.ComponentModel.ISupportInitialize)(this.empimage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.empgrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox empname;
        private System.Windows.Forms.TextBox empemail;
        private System.Windows.Forms.TextBox emppassword;
        private System.Windows.Forms.TextBox empidnum;
        private System.Windows.Forms.ComboBox emprole;
        private System.Windows.Forms.Button Addemployee;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox empimage;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView empgrid;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button Updateempinfo;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ComboBox empgender;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox empmobile;
        private System.Windows.Forms.Label label8;
    }
}