﻿namespace Center
{
    partial class customer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(customer));
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.customersearch = new System.Windows.Forms.TextBox();
            this.custgrid = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button9 = new System.Windows.Forms.Button();
            this.updatecustomer = new System.Windows.Forms.Button();
            this.deletecustomer = new System.Windows.Forms.Button();
            this.addcustomer = new System.Windows.Forms.Button();
            this.newcustomer = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.custpossition = new System.Windows.Forms.Label();
            this.prevouscustomer = new System.Windows.Forms.Button();
            this.nextcustomer = new System.Windows.Forms.Button();
            this.lastcustomer = new System.Windows.Forms.Button();
            this.firstcustomer = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.custpos = new System.Windows.Forms.Label();
            this.last = new System.Windows.Forms.Button();
            this.next = new System.Windows.Forms.Button();
            this.prevous = new System.Windows.Forms.Button();
            this.first = new System.Windows.Forms.Button();
            this.customerimg = new System.Windows.Forms.PictureBox();
            this.Custfirstname = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.custemail = new System.Windows.Forms.TextBox();
            this.custmobile = new System.Windows.Forms.TextBox();
            this.custlastname = new System.Windows.Forms.TextBox();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.custgrid)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.customerimg)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.customersearch);
            this.groupBox3.Controls.Add(this.custgrid);
            this.groupBox3.Location = new System.Drawing.Point(457, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(382, 348);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(13, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 19);
            this.label5.TabIndex = 20;
            this.label5.Text = "First Name";
            // 
            // customersearch
            // 
            this.customersearch.Location = new System.Drawing.Point(120, 19);
            this.customersearch.Name = "customersearch";
            this.customersearch.Size = new System.Drawing.Size(155, 20);
            this.customersearch.TabIndex = 21;
            this.customersearch.TextChanged += new System.EventHandler(this.customersearch_TextChanged);
            // 
            // custgrid
            // 
            this.custgrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.custgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.custgrid.Location = new System.Drawing.Point(0, 58);
            this.custgrid.Name = "custgrid";
            this.custgrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.custgrid.Size = new System.Drawing.Size(370, 282);
            this.custgrid.TabIndex = 0;
            this.custgrid.SelectionChanged += new System.EventHandler(this.custgrid_SelectionChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.button9);
            this.groupBox2.Controls.Add(this.updatecustomer);
            this.groupBox2.Controls.Add(this.deletecustomer);
            this.groupBox2.Controls.Add(this.addcustomer);
            this.groupBox2.Controls.Add(this.newcustomer);
            this.groupBox2.Location = new System.Drawing.Point(12, 310);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(428, 76);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(330, 19);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 4;
            this.button9.Text = "Print";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // updatecustomer
            // 
            this.updatecustomer.Location = new System.Drawing.Point(249, 19);
            this.updatecustomer.Name = "updatecustomer";
            this.updatecustomer.Size = new System.Drawing.Size(75, 23);
            this.updatecustomer.TabIndex = 3;
            this.updatecustomer.Text = "Update";
            this.updatecustomer.UseVisualStyleBackColor = true;
            this.updatecustomer.Click += new System.EventHandler(this.updatecustomer_Click);
            // 
            // deletecustomer
            // 
            this.deletecustomer.Location = new System.Drawing.Point(168, 19);
            this.deletecustomer.Name = "deletecustomer";
            this.deletecustomer.Size = new System.Drawing.Size(75, 23);
            this.deletecustomer.TabIndex = 2;
            this.deletecustomer.Text = "Delete";
            this.deletecustomer.UseVisualStyleBackColor = true;
            this.deletecustomer.Click += new System.EventHandler(this.deletecustomer_Click);
            // 
            // addcustomer
            // 
            this.addcustomer.Enabled = false;
            this.addcustomer.Location = new System.Drawing.Point(87, 19);
            this.addcustomer.Name = "addcustomer";
            this.addcustomer.Size = new System.Drawing.Size(75, 23);
            this.addcustomer.TabIndex = 1;
            this.addcustomer.Text = "Add";
            this.addcustomer.UseVisualStyleBackColor = true;
            this.addcustomer.Click += new System.EventHandler(this.addcustomer_Click);
            // 
            // newcustomer
            // 
            this.newcustomer.Location = new System.Drawing.Point(6, 19);
            this.newcustomer.Name = "newcustomer";
            this.newcustomer.Size = new System.Drawing.Size(75, 23);
            this.newcustomer.TabIndex = 0;
            this.newcustomer.Text = "New";
            this.newcustomer.UseVisualStyleBackColor = true;
            this.newcustomer.Click += new System.EventHandler(this.newcustomer_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.custpossition);
            this.groupBox1.Controls.Add(this.prevouscustomer);
            this.groupBox1.Controls.Add(this.nextcustomer);
            this.groupBox1.Controls.Add(this.lastcustomer);
            this.groupBox1.Controls.Add(this.firstcustomer);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.custpos);
            this.groupBox1.Controls.Add(this.last);
            this.groupBox1.Controls.Add(this.next);
            this.groupBox1.Controls.Add(this.prevous);
            this.groupBox1.Controls.Add(this.first);
            this.groupBox1.Controls.Add(this.customerimg);
            this.groupBox1.Controls.Add(this.Custfirstname);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.custemail);
            this.groupBox1.Controls.Add(this.custmobile);
            this.groupBox1.Controls.Add(this.custlastname);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(439, 292);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // custpossition
            // 
            this.custpossition.AutoSize = true;
            this.custpossition.BackColor = System.Drawing.Color.Transparent;
            this.custpossition.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.custpossition.Location = new System.Drawing.Point(164, 240);
            this.custpossition.Name = "custpossition";
            this.custpossition.Size = new System.Drawing.Size(72, 19);
            this.custpossition.TabIndex = 34;
            this.custpossition.Text = "Possition";
            // 
            // prevouscustomer
            // 
            this.prevouscustomer.Location = new System.Drawing.Point(71, 240);
            this.prevouscustomer.Name = "prevouscustomer";
            this.prevouscustomer.Size = new System.Drawing.Size(75, 23);
            this.prevouscustomer.TabIndex = 33;
            this.prevouscustomer.Text = "<";
            this.prevouscustomer.UseVisualStyleBackColor = true;
            this.prevouscustomer.Click += new System.EventHandler(this.prevouscustomer_Click);
            // 
            // nextcustomer
            // 
            this.nextcustomer.Location = new System.Drawing.Point(261, 240);
            this.nextcustomer.Name = "nextcustomer";
            this.nextcustomer.Size = new System.Drawing.Size(75, 23);
            this.nextcustomer.TabIndex = 32;
            this.nextcustomer.Text = ">";
            this.nextcustomer.UseVisualStyleBackColor = true;
            this.nextcustomer.Click += new System.EventHandler(this.nextcustomer_Click);
            // 
            // lastcustomer
            // 
            this.lastcustomer.Location = new System.Drawing.Point(330, 240);
            this.lastcustomer.Name = "lastcustomer";
            this.lastcustomer.Size = new System.Drawing.Size(75, 23);
            this.lastcustomer.TabIndex = 31;
            this.lastcustomer.Text = ">>";
            this.lastcustomer.UseVisualStyleBackColor = true;
            this.lastcustomer.Click += new System.EventHandler(this.lastcustomer_Click);
            // 
            // firstcustomer
            // 
            this.firstcustomer.Location = new System.Drawing.Point(0, 240);
            this.firstcustomer.Name = "firstcustomer";
            this.firstcustomer.Size = new System.Drawing.Size(75, 23);
            this.firstcustomer.TabIndex = 30;
            this.firstcustomer.Text = "<<";
            this.firstcustomer.UseVisualStyleBackColor = true;
            this.firstcustomer.Click += new System.EventHandler(this.firstcustomer_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(290, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 19);
            this.label6.TabIndex = 24;
            this.label6.Text = "Image";
            // 
            // custpos
            // 
            this.custpos.Location = new System.Drawing.Point(0, 0);
            this.custpos.Name = "custpos";
            this.custpos.Size = new System.Drawing.Size(100, 23);
            this.custpos.TabIndex = 35;
            // 
            // last
            // 
            this.last.Location = new System.Drawing.Point(0, 0);
            this.last.Name = "last";
            this.last.Size = new System.Drawing.Size(75, 23);
            this.last.TabIndex = 36;
            // 
            // next
            // 
            this.next.Location = new System.Drawing.Point(0, 0);
            this.next.Name = "next";
            this.next.Size = new System.Drawing.Size(75, 23);
            this.next.TabIndex = 37;
            // 
            // prevous
            // 
            this.prevous.Location = new System.Drawing.Point(0, 0);
            this.prevous.Name = "prevous";
            this.prevous.Size = new System.Drawing.Size(75, 23);
            this.prevous.TabIndex = 38;
            // 
            // first
            // 
            this.first.Location = new System.Drawing.Point(0, 0);
            this.first.Name = "first";
            this.first.Size = new System.Drawing.Size(75, 23);
            this.first.TabIndex = 39;
            // 
            // customerimg
            // 
            this.customerimg.BackColor = System.Drawing.Color.LightGray;
            this.customerimg.Cursor = System.Windows.Forms.Cursors.Hand;
            this.customerimg.Location = new System.Drawing.Point(263, 51);
            this.customerimg.Name = "customerimg";
            this.customerimg.Size = new System.Drawing.Size(142, 183);
            this.customerimg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.customerimg.TabIndex = 12;
            this.customerimg.TabStop = false;
            this.customerimg.Click += new System.EventHandler(this.customerimg_Click);
            // 
            // Custfirstname
            // 
            this.Custfirstname.Location = new System.Drawing.Point(87, 29);
            this.Custfirstname.Name = "Custfirstname";
            this.Custfirstname.Size = new System.Drawing.Size(121, 20);
            this.Custfirstname.TabIndex = 0;
            this.Custfirstname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Custfirstname_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(-4, 191);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 19);
            this.label4.TabIndex = 17;
            this.label4.Text = "Email";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(-4, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 19);
            this.label3.TabIndex = 16;
            this.label3.Text = "Mobile";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(-4, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 19);
            this.label2.TabIndex = 15;
            this.label2.Text = "Last Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(-4, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 19);
            this.label1.TabIndex = 14;
            this.label1.Text = "First Name";
            // 
            // custemail
            // 
            this.custemail.Location = new System.Drawing.Point(87, 193);
            this.custemail.Name = "custemail";
            this.custemail.Size = new System.Drawing.Size(121, 20);
            this.custemail.TabIndex = 3;
            this.custemail.KeyDown += new System.Windows.Forms.KeyEventHandler(this.custemail_KeyDown);
            // 
            // custmobile
            // 
            this.custmobile.Location = new System.Drawing.Point(87, 140);
            this.custmobile.Name = "custmobile";
            this.custmobile.Size = new System.Drawing.Size(121, 20);
            this.custmobile.TabIndex = 2;
            this.custmobile.KeyDown += new System.Windows.Forms.KeyEventHandler(this.custmobile_KeyDown);
            // 
            // custlastname
            // 
            this.custlastname.Location = new System.Drawing.Point(87, 87);
            this.custlastname.Name = "custlastname";
            this.custlastname.Size = new System.Drawing.Size(121, 20);
            this.custlastname.TabIndex = 1;
            this.custlastname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.custlastname_KeyDown);
            // 
            // customer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(838, 389);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "customer";
            this.Text = "customer";
            this.Load += new System.EventHandler(this.customer_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.custgrid)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.customerimg)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView custgrid;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button updatecustomer;
        private System.Windows.Forms.Button deletecustomer;
        private System.Windows.Forms.Button addcustomer;
        private System.Windows.Forms.Button newcustomer;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox Custfirstname;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox custemail;
        private System.Windows.Forms.TextBox custmobile;
        private System.Windows.Forms.TextBox custlastname;
        private System.Windows.Forms.PictureBox customerimg;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox customersearch;
        private System.Windows.Forms.Label custpos;
        private System.Windows.Forms.Button last;
        private System.Windows.Forms.Button next;
        private System.Windows.Forms.Button prevous;
        private System.Windows.Forms.Button first;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button firstcustomer;
        private System.Windows.Forms.Label custpossition;
        private System.Windows.Forms.Button prevouscustomer;
        private System.Windows.Forms.Button nextcustomer;
        private System.Windows.Forms.Button lastcustomer;
    }
}