﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary1;
using System.IO;

namespace Center
{
    public partial class register : Form
    {
        public register()
        {
            InitializeComponent();
        }
        public void employeegrid()
        {
            using (var _db = new Datacontext())
            {
                var data = (from emp in _db._Employee
                            select new
                            {
                                EmployeeId = emp.ID,
                                Name = emp.name,
                                Identynumber = emp.IDentitynu,
                                emp.Mobile,
                                Pasoord = emp.Passwor,
                                emp.Gender
                            }).ToList();
                empgrid.DataSource = data;
            }
        }
        //add employee
        private void Addnem_Click(object sender, EventArgs e)
        {
            if (empname.Text == string.Empty || empemail.Text == string.Empty || emppassword.Text == string.Empty || empidnum.Text == string.Empty || emprole.Text == string.Empty || empgender.Text == string.Empty)
            {
                MessageBox.Show("you can keep the employee mobile or image empty only ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            else
            {
                using (var _db = new Datacontext())
                {
                    if (empimage.Image != null)
                    {
                        MemoryStream m = new MemoryStream();
                        empimage.Image.Save(m, empimage.Image.RawFormat);
                        byte[] byteimg = m.ToArray();
                        Random emid = new Random();
                    Start:
                        int emid1 = emid.Next(1000, 9999);
                        Employee ex = _db._Employee.SingleOrDefault(p => p.ID == emid1);
                        while (ex != null)
                        {
                            goto Start;
                        }
                        Employee e1 = new Employee
                        {
                            ID = emid1,
                            name = empname.Text,
                            email = empemail.Text,
                            Passwor = emppassword.Text,
                            Role = emprole.SelectedItem.ToString(),
                            IDentitynu = empidnum.Text,
                            Gender = empgender.SelectedItem.ToString(),
                            Mobile = empmobile.Text,
                            image = byteimg
                        };
                        _db._Employee.Add(e1);
                        _db.SaveChanges();
                        MessageBox.Show("The Emplpyee has added", "Add Employee", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        employeegrid();


                    }
                    else
                    {
                        Random emid = new Random();
                    Start:
                        int emid1 = emid.Next(1000, 9999);
                        Employee ex = _db._Employee.SingleOrDefault(p => p.ID == emid1);
                        while (ex != null)
                        {
                            goto Start;
                        }
                        Employee e1 = new Employee
                        {
                            ID = emid1,
                            name = empname.Text,
                            email = empemail.Text,
                            Passwor = emppassword.Text,
                            Role = emprole.Text.ToString(),
                            IDentitynu = empidnum.Text,
                            Gender = empgender.SelectedItem.ToString(),
                            Mobile = empmobile.Text,
                        };
                        _db._Employee.Add(e1);
                        _db.SaveChanges();
                        MessageBox.Show("The Emplpyee has added", "Add Employee", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        employeegrid();
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog opf = new OpenFileDialog();
            opf.Filter = "image files|*.jpg;*.png;*.gif;*.icon";
            DialogResult dr = opf.ShowDialog();


            if (dr == DialogResult.Cancel)
                return;
            empimage.Image = Image.FromFile(opf.FileName);
            empimage.SizeMode = PictureBoxSizeMode.StretchImage;
            empimage.BorderStyle = BorderStyle.Fixed3D;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (var _db = new Datacontext())
            {
                var em = _db._Employee.ToList();
                empgrid.DataSource = em;
            }
            
            
        }
        //remove Employee
        private void button3_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("are you sure the you want to delete this Employee", "delete Employee", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                using (var _db = new Datacontext())
                {
                    int id = Convert.ToInt32(empgrid.CurrentRow.Cells[0].Value);
                    Employee emp = _db._Employee.SingleOrDefault(c => c.ID == id);
                    _db._Employee.Remove(emp);
                    _db.SaveChanges();
                    MessageBox.Show("item has deleted", "delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    employeegrid();
                }
        }
            
        //Update the Employee information
        private void button4_Click(object sender, EventArgs e)
        {
            using (var _db = new Datacontext())
            {
                MemoryStream m = new MemoryStream();
                empimage.Image.Save(m, empimage.Image.RawFormat);
                byte[] byteimg = m.ToArray();
                int id = Convert.ToInt32(empgrid.CurrentRow.Cells[0].Value);
                var emp = _db._Employee.SingleOrDefault(p => p.ID == id);
                emp.name = empname.Text;
                emp.email = empemail.Text;
                emp.Passwor = emppassword.Text;
                emp.Role = emprole.SelectedItem.ToString();
                emp.Gender = empgender.SelectedItem.ToString();
                emp.IDentitynu = empidnum.Text;
                emp.image = byteimg;
                emp.Mobile = empmobile.Text;
                _db.SaveChanges();
                MessageBox.Show("Updated!","Edit Employee Information",MessageBoxButtons.OK);
                employeegrid();
            }
        }

            
           
                

      
 
        private void clearText()
        {
            emppassword.Clear();
            empidnum.Clear();
            empname.Clear();
            empemail.Clear();
            emprole.SelectedIndex = 0;
            empgender.SelectedIndex = 0;
            empmobile.Clear();
        }
        
        private void empgrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            using (var _db = new Datacontext())
            {
                int ID = Convert.ToInt32(empgrid.CurrentRow.Cells[0].Value);
                var emp =_db._Employee.SingleOrDefault(p => p.ID == ID);
                empname.Text = emp.name;
                empemail.Text = emp.email;
                empmobile.Text = emp.Mobile;
                empidnum.Text = emp.IDentitynu;
                emprole.Text = emp.Role;
                emppassword.Text = emp.Passwor;
                empgender.Text = emp.Gender;
                Byte[] img = (Byte[])emp.image;
                if (img != null)
                {
                    Stream ms = new MemoryStream(img);
                    empimage.Image = Image.FromStream(ms);
                }
                else
                {
                    empimage.Image = null;
                }
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            
            using (var _db = new Datacontext())
            {
              
                    if (empname.Text != "")
                    {
                    var search = from mn in _db._Employee
                                 where mn.name.Contains(empname.Text) || mn.email.Contains(empname.Text) || mn.Mobile.Contains(empname.Text)
                                 select mn;

                        empgrid.DataSource = search.ToList();
                    }
              
            }
                       
        }

        private void register_Load(object sender, EventArgs e)
        {
            employeegrid();
        }

        private void empemail_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void empmobile_KeyPress(object sender, KeyPressEventArgs e)
        {
            char c = e.KeyChar;
            if (!char.IsDigit(c) && c != 8 && c != 13)
            {
                e.Handled = true;
                MessageBox.Show("the mobile only can be numer","Error",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);

            }
        }
    }
}
