﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Center
{
    public partial class Bills : Form
    {
        public Bills()
        {
            InitializeComponent();
        }

        private void sales_Load(object sender, EventArgs e)
        {
            firstdaysearchbill.Format = DateTimePickerFormat.Custom;
            firstdaysearchbill.CustomFormat = "MM/dd/yyyy hh:mm:ss";
            lastdaysearchbill.Format = DateTimePickerFormat.Custom;
            lastdaysearchbill.CustomFormat = "MM/dd/yyyy hh:mm:ss";
            using (var _db = new Datacontext())
            {
                var data = (from a in _db._Bills
                            where a.Customer_ID == null 
                            select new
                            {
                                a.ID,
                                a.date,
                                a.employee_name
                            }).ToList();
                notcustomergrid.DataSource = data;
                var data1 = (from a in _db._Bills
                             join b in _db._Customers
                             on a.Customer_ID equals b.ID
                             select new
                             {
                                 a.ID,
                                 a.date,
                                 a.employee_name,
                                 b.firstname,
                                 b.lastname
                             }).ToList();
                Customerbillgrid.DataSource = data1;
            }
        }
        private void printselectedbill_Click(object sender, EventArgs e)
        {
            int bill_id = Convert.ToInt32(Customerbillgrid.CurrentRow.Cells[0].Value);
            RPT.customerbill myproreport = new RPT.customerbill();
            myproreport.SetParameterValue("@bill_id", bill_id);
            RPT.proreport frmreport = new RPT.proreport();
            frmreport.crystalReportViewer1.ReportSource = myproreport;
            frmreport.ShowDialog();

        }

        private void printnotcustbill_Click(object sender, EventArgs e)
        {
            int bill_id = Convert.ToInt32(notcustomergrid.CurrentRow.Cells[0].Value);
            RPT.normalbill myproreport = new RPT.normalbill();
            myproreport.SetParameterValue("@bill_id", bill_id);
            RPT.proreport frmreport = new RPT.proreport();
            frmreport.crystalReportViewer1.ReportSource = myproreport;
            frmreport.ShowDialog();

        }
        private void billcustomersearch_TextChanged(object sender, EventArgs e)
        {
            if (searchbydate.Checked == true)
            {
                using (var _db = new Datacontext())
                {
                    var data1 = (from a in _db._Bills
                                 join b in _db._Customers
                                 on a.Customer_ID equals b.ID
                                 where a.date >= firstdaysearchbill.Value && a.date <= lastdaysearchbill.Value && (b.firstname + " " + b.lastname).Contains(billcustomersearch.Text)
                                 select new
                                 {
                                     a.ID,
                                     a.date,
                                     a.employee_name,
                                     b.firstname,
                                     b.lastname
                                 }).ToList();
                    Customerbillgrid.DataSource = data1;
                }
            }
            else
            {
                using (var _db = new Datacontext())
                {
                    var data1 = (from a in _db._Bills
                                 join b in _db._Customers
                                 on a.Customer_ID equals b.ID
                                 where (b.firstname + " " + b.lastname).Contains(billcustomersearch.Text)
                                 select new
                                 {
                                     a.ID,
                                     a.date,
                                     a.employee_name,
                                     b.firstname,
                                     b.lastname

                                 }).ToList();
                    Customerbillgrid.DataSource = data1;
                }
            }
        }



        private void searchbydate_CheckedChanged(object sender, EventArgs e)
        {
                if (searchbydate.Checked==true)
                {
                    firstdaysearchbill.Enabled = true;
                    lastdaysearchbill.Enabled = true;
                    using (var _db = new Datacontext())
                    {
                    var data = (from a in _db._Bills
                                where a.Customer_ID == null && a.date >= firstdaysearchbill.Value && a.date <= lastdaysearchbill.Value && (a.employee_name.Contains(noncussearch.Text))
                                select new
                                {
                                    a.ID,
                                    a.date,
                                    a.employee_name
                                }).ToList();                                   
                        notcustomergrid.DataSource = data;
                        var data1 = (from a in _db._Bills
                                     join b in _db._Customers
                                     on a.Customer_ID equals b.ID
                                     where a.date >= firstdaysearchbill.Value && a.date <= lastdaysearchbill.Value && (b.firstname + " " + b.lastname).Contains(billcustomersearch.Text)
                                     select new
                                     {
                                         a.ID,
                                         a.date,
                                         a.employee_name,
                                         b.firstname,
                                         b.lastname

                                     }).ToList();
                        Customerbillgrid.DataSource = data1;
                    }
                }
                else
                {
                    using (var _db = new Datacontext())
                    {
                        firstdaysearchbill.Enabled = false;
                        lastdaysearchbill.Enabled = false;
                    var data = (from a in _db._Bills
                                where a.Customer_ID == null && (a.employee_name.Contains(noncussearch.Text))
                                select new
                                {
                                    a.ID,
                                    a.date,
                                    a.employee_name
                                }).ToList();
                    notcustomergrid.DataSource = data;
                    var data1 = (from a in _db._Bills
                                     join b in _db._Customers
                                     on a.Customer_ID equals b.ID
                                     where (b.firstname + " " + b.lastname).Contains(billcustomersearch.Text)
                                     select new
                                     {
                                         a.ID,
                                         a.date,
                                         a.employee_name,
                                         b.firstname,
                                         b.lastname

                                     }).ToList();
                        Customerbillgrid.DataSource = data1;
                    }
                }
            }
           
            
        

        private void firstdaysearchbill_ValueChanged(object sender, EventArgs e)
        {
            using (var _db = new Datacontext())
            {
                var data = (from a in _db._Bills
                            where a.Customer_ID == null && a.date >= firstdaysearchbill.Value && a.date <= lastdaysearchbill.Value && (a.employee_name.Contains(noncussearch.Text))
                            select new
                            {
                                a.ID,
                                a.date,
                                a.employee_name
                            }).ToList();
                notcustomergrid.DataSource = data;
                var data1 = (from a in _db._Bills
                             join b in _db._Customers
                             on a.Customer_ID equals b.ID
                             where  a.date >= firstdaysearchbill.Value && a.date <= lastdaysearchbill.Value && (b.firstname + " " + b.lastname).Contains(billcustomersearch.Text)
                             select new
                             {
                                 a.ID,
                                 a.date,
                                 a.employee_name,
                                 b.firstname,
                                 b.lastname

                             }).ToList();
                Customerbillgrid.DataSource = data1;
            }
        }

        private void lastdaysearchbill_ValueChanged(object sender, EventArgs e)
        {
            using (var _db = new Datacontext())
            {
                var data = (from a in _db._Bills
                            where a.Customer_ID == null && a.date >= firstdaysearchbill.Value && a.date <= lastdaysearchbill.Value && (a.employee_name.Contains(noncussearch.Text))
                            select new
                            {
                                a.ID,
                                a.date,
                                a.employee_name
                            }).ToList();
                notcustomergrid.DataSource = data;
                var data1 = (from a in _db._Bills
                             join b in _db._Customers
                             on a.Customer_ID equals b.ID
                             where a.date >= firstdaysearchbill.Value && a.date <= lastdaysearchbill.Value && (b.firstname + " " + b.lastname).Contains(billcustomersearch.Text)
                             select new
                             {
                                 a.ID,
                                 a.date,
                                 a.employee_name,
                                 b.firstname,
                                 b.lastname

                             }).ToList();
                Customerbillgrid.DataSource = data1;
            }
        }

        private void noncussearch_TextChanged(object sender, EventArgs e)
        {
            if (searchbydate.Checked == true)
            {
                using (var _db = new Datacontext())
                {
                    var data = (from a in _db._Bills
                                where a.Customer_ID == null && a.date >= firstdaysearchbill.Value && a.date <= lastdaysearchbill.Value && (a.employee_name.Contains(noncussearch.Text))
                                select new
                                {
                                    a.ID,
                                    a.date,
                                    a.employee_name
                                }).ToList();
                    notcustomergrid.DataSource = data;
                }
            }
            else
            {
                using (var _db = new Datacontext())
                {
                    var data = (from a in _db._Bills
                                where a.Customer_ID == null && (a.employee_name.Contains(noncussearch.Text))
                                select new
                                {
                                    a.ID,
                                    a.date,
                                    a.employee_name
                                }).ToList();
                    notcustomergrid.DataSource = data;
                }
            }
        }
    }
}