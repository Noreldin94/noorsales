﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Center
{
    public partial class roles : Form
    {
        public roles()
        {
            InitializeComponent();
        }

        private void roles_Load(object sender, EventArgs e)
        {
            using (var _db = new Datacontext())
            {
                var data = (from emp in _db._Employee
                            select new {
                                emp.Role,
                                emp.ID
                            }).Distinct();
                rolescombo.DataSource = data.ToList();
                rolescombo.DisplayMember = "Role";
                rolescombo.ValueMember = "ID";



            }
        }

        private void saveroles_Click(object sender, EventArgs e)
        {
            using (var _db = new Datacontext())
            {

                rolesclass r1 = new rolesclass { };
                r1.role = rolescombo.Text;
                if (Employeecheck.Checked == true)
                    r1.emp = "true";
                if (Employeecheck.Checked == false)
                    r1.emp = "false";
                if (billcheck.Checked == true)
                    r1.bill = "true";
                if (billcheck.Checked == false)
                    r1.bill = "false";
                if (orderscheck.Checked == true)
                    r1.orders = "true";
                if (orderscheck.Checked == false)
                    r1.orders = "false";
                _db._rolesclass.Add(r1);
                _db.SaveChanges();


                

            
            }
        }
    }
}
