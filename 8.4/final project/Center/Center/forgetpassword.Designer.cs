﻿namespace Center
{
    partial class forgetpassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.identitumber = new System.Windows.Forms.TextBox();
            this.newpassword = new System.Windows.Forms.TextBox();
            this.ChangePassord = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Write your Identity Number";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Write new Password";
            // 
            // identitumber
            // 
            this.identitumber.Location = new System.Drawing.Point(39, 55);
            this.identitumber.Name = "identitumber";
            this.identitumber.Size = new System.Drawing.Size(162, 20);
            this.identitumber.TabIndex = 2;
            // 
            // newpassword
            // 
            this.newpassword.Location = new System.Drawing.Point(39, 150);
            this.newpassword.Name = "newpassword";
            this.newpassword.Size = new System.Drawing.Size(162, 20);
            this.newpassword.TabIndex = 3;
            // 
            // ChangePassord
            // 
            this.ChangePassord.Location = new System.Drawing.Point(39, 205);
            this.ChangePassord.Name = "ChangePassord";
            this.ChangePassord.Size = new System.Drawing.Size(75, 23);
            this.ChangePassord.TabIndex = 4;
            this.ChangePassord.Text = "Ok";
            this.ChangePassord.UseVisualStyleBackColor = true;
            this.ChangePassord.Click += new System.EventHandler(this.ChangePassord_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(138, 205);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Cuncel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // forgetpassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 343);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ChangePassord);
            this.Controls.Add(this.newpassword);
            this.Controls.Add(this.identitumber);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "forgetpassword";
            this.Text = "forgetpassword";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox identitumber;
        private System.Windows.Forms.TextBox newpassword;
        private System.Windows.Forms.Button ChangePassord;
        private System.Windows.Forms.Button button1;
    }
}