﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary1;
using System.IO;
using System.Windows.Forms;
using System.Drawing.Imaging;
using CrystalDecisions.Shared;

namespace Center
{
    public partial class Products : Form
    {
        public Products()
        {
            InitializeComponent();
        }
      public void productsgrid()
        {
            using (var _db = new Datacontext())
            {
                var data = (from pro in _db._product
                            select new
                            {
                                ProductID = pro.Id,
                                pro.Name,
                                pro.Type,
                                pro.Price,
                                pro.Quntity,
                                pro.Description   
                            }).ToList();
                progrid.DataSource = data;
            }
        }
        private void Products_Load(object sender, EventArgs e)
        {
            using (var _db = new Datacontext())
            {
                protype.DataSource = _db._Categories.ToList();
                protype.DisplayMember = "Type";
                protype.ValueMember = "Id";
                productsgrid();
            }

        }
        //Add product
        private void button1_Click(object sender, EventArgs e)
        {
            using (var _db = new Datacontext())
            {
                MemoryStream m = new MemoryStream();
                proimg.Image.Save(m, proimg.Image.RawFormat);
                byte[] byteimg = m.ToArray();
                start:
                Random prid = new Random();
                int prid1 = prid.Next(1000, 9999);
                product ex = _db._product.SingleOrDefault(pr => pr.Id == prid1);
                while (ex != null)
                {
                    goto start;
                }
                int id = (from s in _db._Categories
                          where s.Type.Equals(protype.Text.ToString())
                          select s.ID).First();
                
                    product p1 = new product
                    {
                        Id = prid1,
                        Name = proname.Text,
                        Quntity =Convert.ToInt32( proquantity.Text),
                        Price =Convert.ToDouble( proprice.Text),
                        Type = protype.Text,
                        image = byteimg,
                        Description = prodesc.Text,
                        categ_Id = id
                    };
                    _db._product.Add(p1);
                    _db.SaveChanges();
                productsgrid();
                } 
        }

        private void combopro_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        //print selected product
        private void button5_Click(object sender, EventArgs e)
        {
            using (var _db = new Datacontext())
            {
                RPT.selectedproduct myproreport = new RPT.selectedproduct();
                int id = Convert.ToInt32(progrid.CurrentRow.Cells[0].Value);
                 myproreport.SetDataSource(from pro in _db._product
                    where pro.Id == id
                    select new
                    {
                        pro.Id,
                        pro.Name,
                        pro.Price,
                        pro.Quntity,
                        pro.Description,
                        pro.Type,
                        pro.image
                    });                
                RPT.proreport frmreport = new RPT.proreport();
                frmreport.crystalReportViewer1.ReportSource = myproreport;
                frmreport.ShowDialog();
            }
        }
        private void button9_Click(object sender, EventArgs e)
        {
           
        }
        //delete product
        private void button2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("are you sure the you want to delete this item", "delete item", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
            {
                using (var _db = new Datacontext())
                {
                    int id = Convert.ToInt32(progrid.CurrentRow.Cells[0].Value);
                    var pro = _db._product.SingleOrDefault(p => p.Id == id);
                    _db._product.Remove(pro);     
                    MessageBox.Show("item has deleted", "delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _db.SaveChanges();
                    productsgrid();
                }
            }
        }
        //print all product
        private void button6_Click(object sender, EventArgs e)
        {
            RPT.allproduct myproreport = new RPT.allproduct();
            RPT.proreport frmreport = new RPT.proreport();
            frmreport.crystalReportViewer1.ReportSource = myproreport;
            frmreport.ShowDialog();
        }
        //Close
        private void button8_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //save all product
        private void button7_Click(object sender, EventArgs e)
        {

            RPT.allproduct myproreport = new RPT.allproduct();
            ExportOptions exp = new ExportOptions();
            DiskFileDestinationOptions dfop = new DiskFileDestinationOptions();
            ExcelFormatOptions excel = new ExcelFormatOptions();
            dfop.DiskFileName = @"E:\allproduct.xls";
            exp = myproreport.ExportOptions;
            exp.ExportDestinationType = ExportDestinationType.DiskFile;
            exp.ExportFormatType = ExportFormatType.Excel;
            exp.ExportFormatOptions = excel;
            exp.ExportDestinationOptions = dfop;
            myproreport.Export();
            MessageBox.Show("saved", "save file", MessageBoxButtons.OK);
            
            

        }
        private void probrowimg_Click(object sender, EventArgs e)
        {
            OpenFileDialog opf = new OpenFileDialog();
            opf.Filter = "image files|*.jpg;*.png;*.gif;*.icon";
            DialogResult dr = opf.ShowDialog();
            if (dr == DialogResult.Cancel)
                return;
            proimg.Image = Image.FromFile(opf.FileName);
            proimg.SizeMode = PictureBoxSizeMode.StretchImage;
            proimg.BorderStyle = BorderStyle.Fixed3D;
        }

        private void progrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            using (var _db = new Datacontext())
            {
                int ID = Convert.ToInt32(progrid.CurrentRow.Cells[0].Value);
                var pro = _db._product.SingleOrDefault(p => p.Id == ID);
                proname.Text = pro.Name;
                protype.Text = pro.Type;
                proprice.Text =Convert.ToDouble(pro.Price).ToString();
                proquantity.Text = Convert.ToInt32(pro.Quntity).ToString();
                prodesc.Text = pro.Description;
                Byte[] img = (Byte[])pro.image;
                if (img != null)
                {
                    Stream ms = new MemoryStream(img);
                    proimg.Image = Image.FromStream(ms);
                }
                else
                {
                    proimg.Image = null;
                }

            }

        }
        //product photo
        private void button4_Click(object sender, EventArgs e)
        {
            int rowindex = progrid.CurrentCell.RowIndex;
            int columnindex = progrid.CurrentCell.ColumnIndex;
            prophoto frm = new prophoto();
            Byte[] img = (Byte[])progrid.Rows[rowindex].Cells[6].Value;
            Stream ms = new MemoryStream(img);
            frm.prophot.Image = Image.FromStream(ms);
            frm.ShowDialog();


        }
        //Update product
        private void button3_Click(object sender, EventArgs e)
        {
            using (var _db = new Datacontext())
            {
                MemoryStream m = new MemoryStream();
                proimg.Image.Save(m, proimg.Image.RawFormat);
                byte[] byteimg = m.ToArray();
                int rowindex = progrid.CurrentCell.RowIndex;
                int columnindex = progrid.CurrentCell.ColumnIndex;
                int ID = Convert.ToInt32(progrid.Rows[rowindex].Cells[0].Value);
                var pro = _db._product.SingleOrDefault(p => p.Id == ID);
                pro.Name = proname.Text;
                pro.Quntity =Convert.ToInt32( proquantity.Text);
                pro.Price =Convert.ToDouble( proprice.Text);
                pro.Type = protype.SelectedItem.ToString();
                pro.image = byteimg;
                pro.Description = prodesc.Text;
                _db.SaveChanges();
                MessageBox.Show("Updated!","Update",MessageBoxButtons.OK,MessageBoxIcon.Information);
                productsgrid();
            }
        }

        private void pronamesearch_TextChanged(object sender, EventArgs e)
        {
            using (var _db = new Datacontext())
            {              
                    var data = (from pro in _db._product
                                where pro.Name.Contains(pronamesearch.Text)
                                select new {
                                    ProductID=pro.Id,
                                    pro.Name,
                                    pro.Price,
                                    pro.Quntity,
                                    pro.Type
                                }).ToList();
                    progrid.DataSource = data;
            }
        }
        private void proprice_KeyPress(object sender, KeyPressEventArgs e)
        {
            char c = e.KeyChar;
            if (!char.IsDigit(c) && c != 8 && c != 13 && c != Convert.ToChar(System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator))
            {
                e.Handled = true;
                MessageBox.Show("enter the correct vlaue for Price");
            }
        }
        private void proquantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            char c = e.KeyChar;
            if (!char.IsDigit(c) && c != 8 && c != 13)
            {
                e.Handled = true;
                MessageBox.Show("the quantity only can be numer");
            }
        }
    }
}
                   
   
