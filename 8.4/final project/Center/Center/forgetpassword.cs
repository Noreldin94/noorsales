﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Center
{
    public partial class forgetpassword : Form
    {
        public forgetpassword()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ChangePassord_Click(object sender, EventArgs e)
        {
            using (var _db = new Datacontext())
            {
                string identityno = identitumber.Text;
                string pass = newpassword.Text;
                Employee emp = _db._Employee.SingleOrDefault(c => c.IDentitynu ==identityno);
                if(emp!=null)
                {
                    emp.Passwor = pass;
                    _db.SaveChanges();
                    MessageBox.Show("Passord has Changed", "Change Passord", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                MessageBox.Show("Identity Number is Incorrect", "Change Passord", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
    }
}
