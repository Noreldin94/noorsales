﻿namespace Center
{
    partial class Products
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Products));
            this.Addproduct = new System.Windows.Forms.Button();
            this.protype = new System.Windows.Forms.ComboBox();
            this.proname = new System.Windows.Forms.TextBox();
            this.proquantity = new System.Windows.Forms.TextBox();
            this.proprice = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.progrid = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Close = new System.Windows.Forms.Button();
            this.saveallproduct = new System.Windows.Forms.Button();
            this.printallproduct = new System.Windows.Forms.Button();
            this.Printselectproduct = new System.Windows.Forms.Button();
            this.ProductPhoto = new System.Windows.Forms.Button();
            this.Updateproduct = new System.Windows.Forms.Button();
            this.deleteproduct = new System.Windows.Forms.Button();
            this.proimg = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.pronamesearch = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.probrowimg = new System.Windows.Forms.Button();
            this.prodesc = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.progrid)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.proimg)).BeginInit();
            this.SuspendLayout();
            // 
            // Addproduct
            // 
            this.Addproduct.Location = new System.Drawing.Point(42, 19);
            this.Addproduct.Name = "Addproduct";
            this.Addproduct.Size = new System.Drawing.Size(75, 23);
            this.Addproduct.TabIndex = 0;
            this.Addproduct.Text = "Add";
            this.Addproduct.UseVisualStyleBackColor = true;
            this.Addproduct.Click += new System.EventHandler(this.button1_Click);
            // 
            // protype
            // 
            this.protype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.protype.FormattingEnabled = true;
            this.protype.Location = new System.Drawing.Point(105, 28);
            this.protype.Name = "protype";
            this.protype.Size = new System.Drawing.Size(121, 21);
            this.protype.TabIndex = 0;
            this.protype.SelectedIndexChanged += new System.EventHandler(this.combopro_SelectedIndexChanged);
            // 
            // proname
            // 
            this.proname.Location = new System.Drawing.Point(105, 82);
            this.proname.Name = "proname";
            this.proname.Size = new System.Drawing.Size(121, 20);
            this.proname.TabIndex = 1;
            // 
            // proquantity
            // 
            this.proquantity.Location = new System.Drawing.Point(105, 136);
            this.proquantity.Name = "proquantity";
            this.proquantity.Size = new System.Drawing.Size(121, 20);
            this.proquantity.TabIndex = 2;
            this.proquantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.proquantity_KeyPress);
            // 
            // proprice
            // 
            this.proprice.Location = new System.Drawing.Point(105, 191);
            this.proprice.Name = "proprice";
            this.proprice.Size = new System.Drawing.Size(121, 20);
            this.proprice.TabIndex = 3;
            this.proprice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.proprice_KeyPress);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.progrid);
            this.groupBox1.Location = new System.Drawing.Point(260, 101);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(587, 234);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Products info";
            // 
            // progrid
            // 
            this.progrid.AllowUserToAddRows = false;
            this.progrid.AllowUserToDeleteRows = false;
            this.progrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.progrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.progrid.Location = new System.Drawing.Point(6, 19);
            this.progrid.MultiSelect = false;
            this.progrid.Name = "progrid";
            this.progrid.ReadOnly = true;
            this.progrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.progrid.Size = new System.Drawing.Size(581, 209);
            this.progrid.TabIndex = 0;
            this.progrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.progrid_CellClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 19);
            this.label1.TabIndex = 6;
            this.label1.Text = "Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 19);
            this.label2.TabIndex = 7;
            this.label2.Text = "Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 19);
            this.label3.TabIndex = 8;
            this.label3.Text = "Quantity";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1, 189);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 19);
            this.label4.TabIndex = 9;
            this.label4.Text = "Price";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.Close);
            this.groupBox2.Controls.Add(this.saveallproduct);
            this.groupBox2.Controls.Add(this.printallproduct);
            this.groupBox2.Controls.Add(this.Printselectproduct);
            this.groupBox2.Controls.Add(this.ProductPhoto);
            this.groupBox2.Controls.Add(this.Updateproduct);
            this.groupBox2.Controls.Add(this.deleteproduct);
            this.groupBox2.Controls.Add(this.Addproduct);
            this.groupBox2.Location = new System.Drawing.Point(338, 343);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(431, 80);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // Close
            // 
            this.Close.Location = new System.Drawing.Point(348, 51);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(75, 23);
            this.Close.TabIndex = 6;
            this.Close.Text = "Close";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.button8_Click);
            // 
            // saveallproduct
            // 
            this.saveallproduct.Location = new System.Drawing.Point(267, 51);
            this.saveallproduct.Name = "saveallproduct";
            this.saveallproduct.Size = new System.Drawing.Size(75, 23);
            this.saveallproduct.TabIndex = 5;
            this.saveallproduct.Text = "Save As";
            this.saveallproduct.UseVisualStyleBackColor = true;
            this.saveallproduct.Click += new System.EventHandler(this.button7_Click);
            // 
            // printallproduct
            // 
            this.printallproduct.Location = new System.Drawing.Point(105, 51);
            this.printallproduct.Name = "printallproduct";
            this.printallproduct.Size = new System.Drawing.Size(156, 23);
            this.printallproduct.TabIndex = 4;
            this.printallproduct.Text = "Print all the products";
            this.printallproduct.UseVisualStyleBackColor = true;
            this.printallproduct.Click += new System.EventHandler(this.button6_Click);
            // 
            // Printselectproduct
            // 
            this.Printselectproduct.Location = new System.Drawing.Point(303, 19);
            this.Printselectproduct.Name = "Printselectproduct";
            this.Printselectproduct.Size = new System.Drawing.Size(108, 23);
            this.Printselectproduct.TabIndex = 3;
            this.Printselectproduct.Text = "Print the product";
            this.Printselectproduct.UseVisualStyleBackColor = true;
            this.Printselectproduct.Click += new System.EventHandler(this.button5_Click);
            // 
            // ProductPhoto
            // 
            this.ProductPhoto.Location = new System.Drawing.Point(204, 19);
            this.ProductPhoto.Name = "ProductPhoto";
            this.ProductPhoto.Size = new System.Drawing.Size(93, 23);
            this.ProductPhoto.TabIndex = 2;
            this.ProductPhoto.Text = "Product photo";
            this.ProductPhoto.UseVisualStyleBackColor = true;
            this.ProductPhoto.Click += new System.EventHandler(this.button4_Click);
            // 
            // Updateproduct
            // 
            this.Updateproduct.Location = new System.Drawing.Point(123, 19);
            this.Updateproduct.Name = "Updateproduct";
            this.Updateproduct.Size = new System.Drawing.Size(75, 23);
            this.Updateproduct.TabIndex = 1;
            this.Updateproduct.Text = "Update";
            this.Updateproduct.UseVisualStyleBackColor = true;
            this.Updateproduct.Click += new System.EventHandler(this.button3_Click);
            // 
            // deleteproduct
            // 
            this.deleteproduct.Location = new System.Drawing.Point(24, 51);
            this.deleteproduct.Name = "deleteproduct";
            this.deleteproduct.Size = new System.Drawing.Size(75, 23);
            this.deleteproduct.TabIndex = 0;
            this.deleteproduct.Text = "Delete";
            this.deleteproduct.UseVisualStyleBackColor = true;
            this.deleteproduct.Click += new System.EventHandler(this.button2_Click);
            // 
            // proimg
            // 
            this.proimg.Location = new System.Drawing.Point(105, 322);
            this.proimg.Name = "proimg";
            this.proimg.Size = new System.Drawing.Size(121, 101);
            this.proimg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.proimg.TabIndex = 11;
            this.proimg.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(1, 377);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 19);
            this.label5.TabIndex = 12;
            this.label5.Text = "Image";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(602, 72);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 13;
            this.button9.Text = "search";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // pronamesearch
            // 
            this.pronamesearch.Location = new System.Drawing.Point(440, 74);
            this.pronamesearch.Name = "pronamesearch";
            this.pronamesearch.Size = new System.Drawing.Size(156, 20);
            this.pronamesearch.TabIndex = 6;
            this.pronamesearch.TextChanged += new System.EventHandler(this.pronamesearch_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(354, 74);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 19);
            this.label6.TabIndex = 15;
            this.label6.Text = "Prodict ID";
            // 
            // probrowimg
            // 
            this.probrowimg.Location = new System.Drawing.Point(130, 431);
            this.probrowimg.Name = "probrowimg";
            this.probrowimg.Size = new System.Drawing.Size(75, 23);
            this.probrowimg.TabIndex = 5;
            this.probrowimg.Text = "Browse";
            this.probrowimg.UseVisualStyleBackColor = true;
            this.probrowimg.Click += new System.EventHandler(this.probrowimg_Click);
            // 
            // prodesc
            // 
            this.prodesc.Location = new System.Drawing.Point(105, 243);
            this.prodesc.Multiline = true;
            this.prodesc.Name = "prodesc";
            this.prodesc.Size = new System.Drawing.Size(121, 59);
            this.prodesc.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(2, 263);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 19);
            this.label7.TabIndex = 18;
            this.label7.Text = "Description";
            // 
            // Products
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(873, 497);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.prodesc);
            this.Controls.Add(this.probrowimg);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pronamesearch);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.proimg);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.proprice);
            this.Controls.Add(this.proquantity);
            this.Controls.Add(this.proname);
            this.Controls.Add(this.protype);
            this.Name = "Products";
            this.Text = "Products";
            this.Load += new System.EventHandler(this.Products_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.progrid)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.proimg)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Addproduct;
        private System.Windows.Forms.ComboBox protype;
        private System.Windows.Forms.TextBox proname;
        private System.Windows.Forms.TextBox proquantity;
        private System.Windows.Forms.TextBox proprice;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView progrid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button printallproduct;
        private System.Windows.Forms.Button Printselectproduct;
        private System.Windows.Forms.Button ProductPhoto;
        private System.Windows.Forms.Button Updateproduct;
        private System.Windows.Forms.Button deleteproduct;
        private System.Windows.Forms.Button saveallproduct;
        private System.Windows.Forms.Button Close;
        private System.Windows.Forms.PictureBox proimg;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TextBox pronamesearch;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button probrowimg;
        private System.Windows.Forms.TextBox prodesc;
        private System.Windows.Forms.Label label7;
    }
}