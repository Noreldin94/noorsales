﻿namespace Center
{
    partial class All_Products
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.allproductbill = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.allproductbill)).BeginInit();
            this.SuspendLayout();
            // 
            // allproductbill
            // 
            this.allproductbill.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.allproductbill.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.allproductbill.Location = new System.Drawing.Point(18, 101);
            this.allproductbill.Name = "allproductbill";
            this.allproductbill.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.allproductbill.Size = new System.Drawing.Size(601, 307);
            this.allproductbill.TabIndex = 1;
            this.allproductbill.DoubleClick += new System.EventHandler(this.allproductbill_DoubleClick);
            // 
            // All_Products
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(636, 413);
            this.Controls.Add(this.allproductbill);
            this.Name = "All_Products";
            this.Text = "All_Products";
            this.Load += new System.EventHandler(this.All_Products_Load);
            ((System.ComponentModel.ISupportInitialize)(this.allproductbill)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.DataGridView allproductbill;
    }
}