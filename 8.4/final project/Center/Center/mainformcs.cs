﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Center
{
    public partial class mainformcs : Form
    {
        public string employeename;
        public string employeerole;
        public int empidno;
        public mainformcs(string username,string role,int empid)
        {
            employeename = username;
            employeerole = role;
            empidno = empid;
            InitializeComponent();
        }

        private void newem_Click(object sender, EventArgs e)
        {
            register frm = new register();
            frm.ShowDialog();
        }

        private void mainformcs_Load(object sender, EventArgs e)
        {

            using (var _db = new Datacontext())
            {
                Employee emp = _db._Employee.SingleOrDefault(p => p.ID == empidno);
                mainfirmempname.Text = emp.name;
                Byte[] img = (Byte[])emp.image;
                if (img != null)
                {
                    Stream ms = new MemoryStream(img);
                    mainfirmemppic.Image = Image.FromStream(ms);
                }
                else
                {
                    mainfirmemppic.Image = null;
                }
                if (employeerole == "Admin")
                {
                    Billsform.Enabled = true;
                    newem.Enabled = true;
                    ordersform.Enabled = true;

                }
                rolesclass r1 = _db._rolesclass.SingleOrDefault(p => p.role == employeerole);
                if( r1 != null)
                {
                    if (r1.bill == "true")
                    {
                        Billsform.Enabled = true;
                    }
                    if (r1.emp == "true")
                    {
                        newem.Enabled = true;
                    }
                    if (r1.orders == "true")
                    {
                        ordersform.Enabled = true;
                    }
                }
            }

                

            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Products product = new Products();
            product.ShowDialog();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            categories frm = new categories();
            frm.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            customer frm = new customer();
            frm.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Order ord = new Order(employeename);
            ord.ShowDialog();
            

        }

        private void Billsform_Click(object sender, EventArgs e)
        {
            Bills frm = new Bills();
            frm.ShowDialog();
        }

        private void salesform_Click(object sender, EventArgs e)
        {
            sales frm = new sales();
            frm.ShowDialog();
        }

        private void roles_Click(object sender, EventArgs e)
        {
            roles frm = new roles();
            frm.ShowDialog();
        }
    }
}
