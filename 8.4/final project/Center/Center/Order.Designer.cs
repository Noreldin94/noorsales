﻿namespace Center
{
    partial class Order
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.custbillid = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.customersbill = new System.Windows.Forms.Button();
            this.custpos = new System.Windows.Forms.Label();
            this.last = new System.Windows.Forms.Button();
            this.next = new System.Windows.Forms.Button();
            this.prevous = new System.Windows.Forms.Button();
            this.first = new System.Windows.Forms.Button();
            this.Custfirstname = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.custemail = new System.Windows.Forms.TextBox();
            this.custmobile = new System.Windows.Forms.TextBox();
            this.custlastname = new System.Windows.Forms.TextBox();
            this.custbillimg = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.billboxid = new System.Windows.Forms.TextBox();
            this.datebill = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.billempl = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.allproductbillbtn = new System.Windows.Forms.Button();
            this.prodtotal = new System.Windows.Forms.TextBox();
            this.proddisc = new System.Windows.Forms.TextBox();
            this.prodamount = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.prodquantity = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.prodprice = new System.Windows.Forms.TextBox();
            this.prodname = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.prodid = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.billgrid = new System.Windows.Forms.DataGridView();
            this.sum = new System.Windows.Forms.Label();
            this.saveBill = new System.Windows.Forms.Button();
            this.print_bill = new System.Windows.Forms.Button();
            this.New_Order = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.custbillimg)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.billgrid)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.custbillid);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.customersbill);
            this.groupBox1.Controls.Add(this.custpos);
            this.groupBox1.Controls.Add(this.last);
            this.groupBox1.Controls.Add(this.next);
            this.groupBox1.Controls.Add(this.prevous);
            this.groupBox1.Controls.Add(this.first);
            this.groupBox1.Controls.Add(this.Custfirstname);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.custemail);
            this.groupBox1.Controls.Add(this.custmobile);
            this.groupBox1.Controls.Add(this.custlastname);
            this.groupBox1.Location = new System.Drawing.Point(16, 155);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(247, 324);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // custbillid
            // 
            this.custbillid.Location = new System.Drawing.Point(111, 57);
            this.custbillid.Name = "custbillid";
            this.custbillid.ReadOnly = true;
            this.custbillid.Size = new System.Drawing.Size(121, 20);
            this.custbillid.TabIndex = 42;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 19);
            this.label5.TabIndex = 41;
            this.label5.Text = "Customer ID";
            // 
            // customersbill
            // 
            this.customersbill.Location = new System.Drawing.Point(6, 29);
            this.customersbill.Name = "customersbill";
            this.customersbill.Size = new System.Drawing.Size(75, 23);
            this.customersbill.TabIndex = 40;
            this.customersbill.Text = "All Customer";
            this.customersbill.UseVisualStyleBackColor = true;
            this.customersbill.Click += new System.EventHandler(this.customersbill_Click);
            // 
            // custpos
            // 
            this.custpos.Location = new System.Drawing.Point(0, 0);
            this.custpos.Name = "custpos";
            this.custpos.Size = new System.Drawing.Size(100, 14);
            this.custpos.TabIndex = 35;
            // 
            // last
            // 
            this.last.Location = new System.Drawing.Point(0, 0);
            this.last.Name = "last";
            this.last.Size = new System.Drawing.Size(75, 14);
            this.last.TabIndex = 36;
            // 
            // next
            // 
            this.next.Location = new System.Drawing.Point(0, 0);
            this.next.Name = "next";
            this.next.Size = new System.Drawing.Size(75, 14);
            this.next.TabIndex = 37;
            // 
            // prevous
            // 
            this.prevous.Location = new System.Drawing.Point(0, 0);
            this.prevous.Name = "prevous";
            this.prevous.Size = new System.Drawing.Size(75, 14);
            this.prevous.TabIndex = 38;
            // 
            // first
            // 
            this.first.Location = new System.Drawing.Point(0, 0);
            this.first.Name = "first";
            this.first.Size = new System.Drawing.Size(75, 14);
            this.first.TabIndex = 39;
            // 
            // Custfirstname
            // 
            this.Custfirstname.Location = new System.Drawing.Point(111, 83);
            this.Custfirstname.Name = "Custfirstname";
            this.Custfirstname.ReadOnly = true;
            this.Custfirstname.Size = new System.Drawing.Size(121, 20);
            this.Custfirstname.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(9, 161);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 19);
            this.label4.TabIndex = 17;
            this.label4.Text = "Email";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(9, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 19);
            this.label3.TabIndex = 16;
            this.label3.Text = "Mobile";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 19);
            this.label2.TabIndex = 15;
            this.label2.Text = "Last Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 19);
            this.label1.TabIndex = 14;
            this.label1.Text = "First Name";
            // 
            // custemail
            // 
            this.custemail.Location = new System.Drawing.Point(111, 160);
            this.custemail.Name = "custemail";
            this.custemail.ReadOnly = true;
            this.custemail.Size = new System.Drawing.Size(121, 20);
            this.custemail.TabIndex = 3;
            // 
            // custmobile
            // 
            this.custmobile.Location = new System.Drawing.Point(111, 134);
            this.custmobile.Name = "custmobile";
            this.custmobile.ReadOnly = true;
            this.custmobile.Size = new System.Drawing.Size(121, 20);
            this.custmobile.TabIndex = 2;
            // 
            // custlastname
            // 
            this.custlastname.Location = new System.Drawing.Point(111, 108);
            this.custlastname.Name = "custlastname";
            this.custlastname.ReadOnly = true;
            this.custlastname.Size = new System.Drawing.Size(121, 20);
            this.custlastname.TabIndex = 1;
            // 
            // custbillimg
            // 
            this.custbillimg.BackColor = System.Drawing.Color.DarkGray;
            this.custbillimg.Location = new System.Drawing.Point(127, 341);
            this.custbillimg.Name = "custbillimg";
            this.custbillimg.Size = new System.Drawing.Size(121, 138);
            this.custbillimg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.custbillimg.TabIndex = 43;
            this.custbillimg.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(33, 403);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 19);
            this.label6.TabIndex = 24;
            this.label6.Text = "Image";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.billboxid);
            this.groupBox2.Controls.Add(this.datebill);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.button5);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.billempl);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(284, 115);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // billboxid
            // 
            this.billboxid.Location = new System.Drawing.Point(139, 28);
            this.billboxid.Name = "billboxid";
            this.billboxid.ReadOnly = true;
            this.billboxid.Size = new System.Drawing.Size(121, 20);
            this.billboxid.TabIndex = 42;
            // 
            // datebill
            // 
            this.datebill.Checked = false;
            this.datebill.Location = new System.Drawing.Point(139, 54);
            this.datebill.Name = "datebill";
            this.datebill.Size = new System.Drawing.Size(142, 20);
            this.datebill.TabIndex = 41;
            this.datebill.Value = new System.DateTime(2017, 4, 4, 0, 0, 0, 0);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 14);
            this.label7.TabIndex = 35;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(0, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 14);
            this.button2.TabIndex = 36;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(0, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 14);
            this.button3.TabIndex = 37;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(0, 0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 14);
            this.button4.TabIndex = 38;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(0, 0);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 14);
            this.button5.TabIndex = 39;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(9, 81);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(124, 19);
            this.label9.TabIndex = 16;
            this.label9.Text = "Employee Name";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 55);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 19);
            this.label10.TabIndex = 15;
            this.label10.Text = "Last Name";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 19);
            this.label11.TabIndex = 14;
            this.label11.Text = "Bill ID";
            // 
            // billempl
            // 
            this.billempl.Location = new System.Drawing.Point(139, 81);
            this.billempl.Name = "billempl";
            this.billempl.ReadOnly = true;
            this.billempl.Size = new System.Drawing.Size(121, 20);
            this.billempl.TabIndex = 2;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.allproductbillbtn);
            this.groupBox3.Controls.Add(this.prodtotal);
            this.groupBox3.Controls.Add(this.proddisc);
            this.groupBox3.Controls.Add(this.prodamount);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.prodquantity);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.prodprice);
            this.groupBox3.Controls.Add(this.prodname);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.prodid);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Location = new System.Drawing.Point(285, 93);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(604, 70);
            this.groupBox3.TabIndex = 44;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // allproductbillbtn
            // 
            this.allproductbillbtn.Location = new System.Drawing.Point(2, 16);
            this.allproductbillbtn.Name = "allproductbillbtn";
            this.allproductbillbtn.Size = new System.Drawing.Size(75, 54);
            this.allproductbillbtn.TabIndex = 16;
            this.allproductbillbtn.Text = "products";
            this.allproductbillbtn.UseVisualStyleBackColor = true;
            this.allproductbillbtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // prodtotal
            // 
            this.prodtotal.Location = new System.Drawing.Point(518, 50);
            this.prodtotal.Name = "prodtotal";
            this.prodtotal.ReadOnly = true;
            this.prodtotal.Size = new System.Drawing.Size(78, 20);
            this.prodtotal.TabIndex = 15;
            // 
            // proddisc
            // 
            this.proddisc.Location = new System.Drawing.Point(447, 50);
            this.proddisc.MaxLength = 3;
            this.proddisc.Name = "proddisc";
            this.proddisc.Size = new System.Drawing.Size(74, 20);
            this.proddisc.TabIndex = 13;
            this.proddisc.TextChanged += new System.EventHandler(this.proddisc_TextChanged);
            this.proddisc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.proddisc_KeyPress);
            this.proddisc.KeyUp += new System.Windows.Forms.KeyEventHandler(this.proddisc_KeyUp);
            // 
            // prodamount
            // 
            this.prodamount.Location = new System.Drawing.Point(377, 50);
            this.prodamount.Name = "prodamount";
            this.prodamount.ReadOnly = true;
            this.prodamount.Size = new System.Drawing.Size(73, 20);
            this.prodamount.TabIndex = 11;
            // 
            // label17
            // 
            this.label17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label17.Location = new System.Drawing.Point(518, 16);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(78, 31);
            this.label17.TabIndex = 14;
            this.label17.Text = "Total";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // prodquantity
            // 
            this.prodquantity.Location = new System.Drawing.Point(315, 50);
            this.prodquantity.MaxLength = 8;
            this.prodquantity.Name = "prodquantity";
            this.prodquantity.Size = new System.Drawing.Size(64, 20);
            this.prodquantity.TabIndex = 9;
            this.prodquantity.TextChanged += new System.EventHandler(this.prodquantity_TextChanged);
            this.prodquantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.prodquantity_KeyPress);
            this.prodquantity.KeyUp += new System.Windows.Forms.KeyEventHandler(this.prodquantity_KeyUp);
            // 
            // label16
            // 
            this.label16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label16.Location = new System.Drawing.Point(447, 16);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(74, 31);
            this.label16.TabIndex = 12;
            this.label16.Text = "Discount %";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label14.Location = new System.Drawing.Point(315, 16);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(62, 31);
            this.label14.TabIndex = 8;
            this.label14.Text = "Quantity";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label15.Location = new System.Drawing.Point(377, 16);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(73, 31);
            this.label15.TabIndex = 10;
            this.label15.Text = "Amount";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // prodprice
            // 
            this.prodprice.Location = new System.Drawing.Point(253, 50);
            this.prodprice.MaxLength = 8;
            this.prodprice.Name = "prodprice";
            this.prodprice.ReadOnly = true;
            this.prodprice.Size = new System.Drawing.Size(65, 20);
            this.prodprice.TabIndex = 7;
            // 
            // prodname
            // 
            this.prodname.Location = new System.Drawing.Point(148, 50);
            this.prodname.Name = "prodname";
            this.prodname.ReadOnly = true;
            this.prodname.Size = new System.Drawing.Size(106, 20);
            this.prodname.TabIndex = 5;
            // 
            // label12
            // 
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label12.Location = new System.Drawing.Point(148, 16);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(105, 31);
            this.label12.TabIndex = 4;
            this.label12.Text = "Product Name";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label13.Location = new System.Drawing.Point(253, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 31);
            this.label13.TabIndex = 6;
            this.label13.Text = "Price";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // prodid
            // 
            this.prodid.Location = new System.Drawing.Point(77, 50);
            this.prodid.Name = "prodid";
            this.prodid.ReadOnly = true;
            this.prodid.Size = new System.Drawing.Size(74, 20);
            this.prodid.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Location = new System.Drawing.Point(77, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 31);
            this.label8.TabIndex = 0;
            this.label8.Text = "Product_Id";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // billgrid
            // 
            this.billgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.billgrid.ColumnHeadersVisible = false;
            this.billgrid.Location = new System.Drawing.Point(287, 169);
            this.billgrid.MultiSelect = false;
            this.billgrid.Name = "billgrid";
            this.billgrid.ReadOnly = true;
            this.billgrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.billgrid.Size = new System.Drawing.Size(594, 147);
            this.billgrid.TabIndex = 11;
            this.billgrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.billgrid_CellContentClick);
            this.billgrid.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.billgrid_RowsRemoved);
            this.billgrid.DoubleClick += new System.EventHandler(this.billgrid_DoubleClick);
            // 
            // sum
            // 
            this.sum.BackColor = System.Drawing.Color.Transparent;
            this.sum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sum.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.sum.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sum.ForeColor = System.Drawing.Color.DimGray;
            this.sum.Location = new System.Drawing.Point(488, 368);
            this.sum.Name = "sum";
            this.sum.Size = new System.Drawing.Size(204, 66);
            this.sum.TabIndex = 45;
            this.sum.Text = "0";
            this.sum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // saveBill
            // 
            this.saveBill.Location = new System.Drawing.Point(749, 22);
            this.saveBill.Name = "saveBill";
            this.saveBill.Size = new System.Drawing.Size(75, 57);
            this.saveBill.TabIndex = 46;
            this.saveBill.Text = "Save";
            this.saveBill.UseVisualStyleBackColor = true;
            this.saveBill.Click += new System.EventHandler(this.saveBill_Click);
            // 
            // print_bill
            // 
            this.print_bill.Location = new System.Drawing.Point(572, 22);
            this.print_bill.Name = "print_bill";
            this.print_bill.Size = new System.Drawing.Size(75, 57);
            this.print_bill.TabIndex = 47;
            this.print_bill.Text = "Print";
            this.print_bill.UseVisualStyleBackColor = true;
            this.print_bill.Click += new System.EventHandler(this.print_bill_Click);
            // 
            // New_Order
            // 
            this.New_Order.Location = new System.Drawing.Point(453, 22);
            this.New_Order.Name = "New_Order";
            this.New_Order.Size = new System.Drawing.Size(75, 57);
            this.New_Order.TabIndex = 48;
            this.New_Order.Text = "New Order";
            this.New_Order.UseVisualStyleBackColor = true;
            this.New_Order.Click += new System.EventHandler(this.New_Order_Click);
            // 
            // Order
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(901, 492);
            this.Controls.Add(this.New_Order);
            this.Controls.Add(this.print_bill);
            this.Controls.Add(this.saveBill);
            this.Controls.Add(this.sum);
            this.Controls.Add(this.billgrid);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.custbillimg);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.groupBox1);
            this.Name = "Order";
            this.Text = "Order";
            this.Load += new System.EventHandler(this.Order_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.custbillimg)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.billgrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button customersbill;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label custpos;
        private System.Windows.Forms.Button last;
        private System.Windows.Forms.Button next;
        private System.Windows.Forms.Button prevous;
        private System.Windows.Forms.Button first;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker datebill;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox billempl;
        private System.Windows.Forms.TextBox billboxid;
        public System.Windows.Forms.TextBox Custfirstname;
        public System.Windows.Forms.TextBox custemail;
        public System.Windows.Forms.TextBox custmobile;
        public System.Windows.Forms.TextBox custlastname;
        public System.Windows.Forms.TextBox custbillid;
        public System.Windows.Forms.PictureBox custbillimg;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button allproductbillbtn;
        private System.Windows.Forms.TextBox proddisc;
        private System.Windows.Forms.TextBox prodamount;
        private System.Windows.Forms.TextBox prodquantity;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox prodprice;
        private System.Windows.Forms.TextBox prodname;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox prodid;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox prodtotal;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.DataGridView billgrid;
        private System.Windows.Forms.Label sum;
        private System.Windows.Forms.Button saveBill;
        private System.Windows.Forms.Button print_bill;
        private System.Windows.Forms.Button New_Order;
    }
}