﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Center
{
    public partial class sales : Form
    {
        public sales()
        {
            InitializeComponent();
        }

        private void sales_Load(object sender, EventArgs e)
        {
            fisrstdateorders.Format = DateTimePickerFormat.Custom;
            fisrstdateorders.CustomFormat = "MM/dd/yyyy hh:mm:ss";
            lastdateorders.Format = DateTimePickerFormat.Custom;
            lastdateorders.CustomFormat = "MM/dd/yyyy hh:mm:ss";
            using (var _db = new Datacontext())
            {

                var data1 = (from a in _db._Orders
                             join b in _db._Bills
                             on a.Bill_ID equals b.ID
                             select new
                             {
                                 a.order_Id,
                                 BillNo= b.ID,
                                 b.date,
                                 Product_Name=a.productname,
                                 a.Quantity,
                                 a.Price,
                                 a.amount,
                                 a.discount,
                                 a.total_amount
                             }).ToList();
                salesgrid.DataSource = data1;
            }
            sum.Text =
                       (from DataGridViewRow roww in salesgrid.Rows
                        where roww.Cells[7].FormattedValue.ToString() != string.Empty
                        select Convert.ToDouble(roww.Cells[8].FormattedValue)).Sum().ToString();
        }

        private void searchsales_Click(object sender, EventArgs e)
        {
            using (var _db = new Datacontext())
            {

                var data1 = (from a in _db._Orders
                             join b in _db._Bills
                             on a.Bill_ID equals b.ID
                             where b.date >= fisrstdateorders.Value && b.date <= lastdateorders.Value
                             select new
                             {
                                 a.order_Id,
                                 BillNo = b.ID,
                                 b.date,
                                 a.Quantity,
                                 a.Price,
                                 a.amount,
                                 a.discount,
                                 a.total_amount
                             }).ToList();
                salesgrid.DataSource = data1;
                sum.Text =
                       (from DataGridViewRow roww in salesgrid.Rows
                        where roww.Cells[7].FormattedValue.ToString() != string.Empty
                        select Convert.ToDouble(roww.Cells[8].FormattedValue)).Sum().ToString();
            }
        }
    }
}
