﻿namespace Center
{
    partial class Bills
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Customerbillgrid = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.notcustomergrid = new System.Windows.Forms.DataGridView();
            this.firstdaysearchbill = new System.Windows.Forms.DateTimePicker();
            this.lastdaysearchbill = new System.Windows.Forms.DateTimePicker();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.printselectedbill = new System.Windows.Forms.Button();
            this.printnotcustbill = new System.Windows.Forms.Button();
            this.billcustomersearch = new System.Windows.Forms.TextBox();
            this.noncussearch = new System.Windows.Forms.TextBox();
            this.searchbydate = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Customerbillgrid)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.notcustomergrid)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.billcustomersearch);
            this.groupBox1.Controls.Add(this.Customerbillgrid);
            this.groupBox1.Location = new System.Drawing.Point(699, 64);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(447, 396);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // Customerbillgrid
            // 
            this.Customerbillgrid.AllowUserToAddRows = false;
            this.Customerbillgrid.AllowUserToDeleteRows = false;
            this.Customerbillgrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.Customerbillgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Customerbillgrid.Location = new System.Drawing.Point(6, 47);
            this.Customerbillgrid.Name = "Customerbillgrid";
            this.Customerbillgrid.ReadOnly = true;
            this.Customerbillgrid.Size = new System.Drawing.Size(435, 344);
            this.Customerbillgrid.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.noncussearch);
            this.groupBox2.Controls.Add(this.notcustomergrid);
            this.groupBox2.Location = new System.Drawing.Point(12, 64);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(414, 391);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // notcustomergrid
            // 
            this.notcustomergrid.AllowUserToAddRows = false;
            this.notcustomergrid.AllowUserToDeleteRows = false;
            this.notcustomergrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.notcustomergrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.notcustomergrid.Location = new System.Drawing.Point(-13, 47);
            this.notcustomergrid.Name = "notcustomergrid";
            this.notcustomergrid.ReadOnly = true;
            this.notcustomergrid.Size = new System.Drawing.Size(421, 344);
            this.notcustomergrid.TabIndex = 1;
            // 
            // firstdaysearchbill
            // 
            this.firstdaysearchbill.Enabled = false;
            this.firstdaysearchbill.Location = new System.Drawing.Point(6, 87);
            this.firstdaysearchbill.Name = "firstdaysearchbill";
            this.firstdaysearchbill.Size = new System.Drawing.Size(200, 20);
            this.firstdaysearchbill.TabIndex = 2;
            this.firstdaysearchbill.ValueChanged += new System.EventHandler(this.firstdaysearchbill_ValueChanged);
            // 
            // lastdaysearchbill
            // 
            this.lastdaysearchbill.Enabled = false;
            this.lastdaysearchbill.Location = new System.Drawing.Point(6, 171);
            this.lastdaysearchbill.Name = "lastdaysearchbill";
            this.lastdaysearchbill.Size = new System.Drawing.Size(200, 20);
            this.lastdaysearchbill.TabIndex = 3;
            this.lastdaysearchbill.ValueChanged += new System.EventHandler(this.lastdaysearchbill_ValueChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.searchbydate);
            this.groupBox3.Controls.Add(this.firstdaysearchbill);
            this.groupBox3.Controls.Add(this.lastdaysearchbill);
            this.groupBox3.Location = new System.Drawing.Point(432, 111);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(217, 304);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // printselectedbill
            // 
            this.printselectedbill.Location = new System.Drawing.Point(708, 22);
            this.printselectedbill.Name = "printselectedbill";
            this.printselectedbill.Size = new System.Drawing.Size(75, 23);
            this.printselectedbill.TabIndex = 6;
            this.printselectedbill.Text = "Print";
            this.printselectedbill.UseVisualStyleBackColor = true;
            this.printselectedbill.Click += new System.EventHandler(this.printselectedbill_Click);
            // 
            // printnotcustbill
            // 
            this.printnotcustbill.Location = new System.Drawing.Point(221, 22);
            this.printnotcustbill.Name = "printnotcustbill";
            this.printnotcustbill.Size = new System.Drawing.Size(75, 23);
            this.printnotcustbill.TabIndex = 7;
            this.printnotcustbill.Text = "Print";
            this.printnotcustbill.UseVisualStyleBackColor = true;
            this.printnotcustbill.Click += new System.EventHandler(this.printnotcustbill_Click);
            // 
            // billcustomersearch
            // 
            this.billcustomersearch.Location = new System.Drawing.Point(153, 19);
            this.billcustomersearch.Name = "billcustomersearch";
            this.billcustomersearch.Size = new System.Drawing.Size(170, 20);
            this.billcustomersearch.TabIndex = 8;
            this.billcustomersearch.Text = "Customer Name";
            this.billcustomersearch.TextChanged += new System.EventHandler(this.billcustomersearch_TextChanged);
            // 
            // noncussearch
            // 
            this.noncussearch.Location = new System.Drawing.Point(114, 21);
            this.noncussearch.Name = "noncussearch";
            this.noncussearch.Size = new System.Drawing.Size(170, 20);
            this.noncussearch.TabIndex = 9;
            this.noncussearch.Text = "Employee name";
            this.noncussearch.TextChanged += new System.EventHandler(this.noncussearch_TextChanged);
            // 
            // searchbydate
            // 
            this.searchbydate.AutoSize = true;
            this.searchbydate.Location = new System.Drawing.Point(19, 33);
            this.searchbydate.Name = "searchbydate";
            this.searchbydate.Size = new System.Drawing.Size(77, 17);
            this.searchbydate.TabIndex = 8;
            this.searchbydate.Text = "checkBox1";
            this.searchbydate.UseVisualStyleBackColor = true;
            this.searchbydate.CheckedChanged += new System.EventHandler(this.searchbydate_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(33, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 19);
            this.label1.TabIndex = 10;
            this.label1.Text = "Search";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(76, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 19);
            this.label2.TabIndex = 11;
            this.label2.Text = "Search";
            // 
            // Bills
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1114, 461);
            this.Controls.Add(this.printnotcustbill);
            this.Controls.Add(this.printselectedbill);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Name = "Bills";
            this.Text = "sales";
            this.Load += new System.EventHandler(this.sales_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Customerbillgrid)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.notcustomergrid)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView Customerbillgrid;
        private System.Windows.Forms.DataGridView notcustomergrid;
        private System.Windows.Forms.DateTimePicker firstdaysearchbill;
        private System.Windows.Forms.DateTimePicker lastdaysearchbill;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button printselectedbill;
        private System.Windows.Forms.Button printnotcustbill;
        private System.Windows.Forms.TextBox billcustomersearch;
        private System.Windows.Forms.TextBox noncussearch;
        private System.Windows.Forms.CheckBox searchbydate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}