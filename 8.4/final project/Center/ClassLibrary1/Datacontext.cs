﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ClassLibrary1
{
    public class Datacontext : DbContext
    {
        public Datacontext():base("Center")
        {
            Database.SetInitializer<Datacontext>(new DbIn());
        }
        public DbSet<Employee> _Employee { get; set; }
        public DbSet<product> _product { get; set; }
        public DbSet<Categories> _Categories { get; set; }
        public DbSet<Customers> _Customers { get; set; }
        public DbSet<Bills> _Bills { get; set; }
        public DbSet<Orders> _Orders { get; set; }
        public DbSet<rolesclass> _rolesclass { get; set; }





    }
}
