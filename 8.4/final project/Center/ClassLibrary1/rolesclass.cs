﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClassLibrary1
{
    public class rolesclass
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        
        [Key]
        public string role { get; set; }
        public string emp { get; set; }
        public string bill { get; set; }
        public string orders { get; set; }
    }
}
